
__local float f(__local float* local_x)
{
  return sin(*local_x);
}

__kernel void convolution(__global float* x, __constant float* filter, 
                                __global float* y,
                                int rows, int cols, int filterWidth,
                                __local float* local_x, int localHeight, int localWidth)
{
   // padding for filter
   int filterRadius = (filterWidth / 2);
   int padding = filterRadius * 2;
  
   // size of work group in output
   int groupStartCol = get_group_id(0) * get_local_size(0);
   int groupStartRow = get_group_id(1) * get_local_size(1);

   // local ID for work-items
   int localCol = get_local_id(0);
   int localRow = get_local_id(1);

   // global ID of each work-item
   int globalCol = groupStartCol + localCol;
   int globalRow = groupStartRow + localRow;

   // step down rows
   for (int i = localRow; i < localHeight; i += get_local_size(1))
   {
      int currRow = groupStartRow + i;

      // step across columns
      for (int j = localCol; j < localWidth; j += get_local_size(0))
      {
         int currCol = groupStartCol + j;

         // perform copy to local memory
         if (currRow < rows && currCol < cols)
         {
            *(local_x + i * localWidth + j) = *(x + currRow * cols + currCol);
         }
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   // perform convolution
   if ((globalRow < (rows - padding)) && (globalCol < (cols - padding)))
   {
     
     // Calculate right hand-side
     float sum = 0.0; // f(local_x);

     int filterIdx = 0;
     //int offset;

     for (int i = localRow; i < localRow + filterWidth; ++i)
     {
        int offset= i * localWidth + localCol;
        sum += local_x[offset++] * filter[filterIdx++];
        sum += local_x[offset++] * filter[filterIdx++];
        sum += local_x[offset++] * filter[filterIdx++];
     }
     *(y + (globalRow + filterRadius) * cols + (globalCol + filterRadius)) = sum;
    }
}

