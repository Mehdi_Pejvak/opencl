
/*
 * @name   convolution.cc
 * @info   The code implements convolution on a given matrix 
 *         on the GPU by using OpenCL library
 *         matrix 'x' is the matrix contains values of vertices in grids.
 *         matrix 'F' is filter data which implements given scheme on matrix x.
 *
 * @author Mehdi
 * @date   27.12.2019
*/


#define __CL_ENABLE_EXCEPTIONS
#define CL_PROFILING
#define CL_READ4

#define WGX 16
#define WGY 16

#include <iostream>
#include <CL/cl.hpp>
#include <cassert>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <cmath>
#include <iomanip>

#ifndef CL_PROFILING
  #include <chrono>
#endif

#include "CL_ERROR.hpp"

/*
 * the function used to roundup a given value to the closest multiplier of 
 * multiple value
 */
int roundUp(int value, int multiple)
{
  int remainder = value % multiple;

  if(remainder != 0)
  {
    value += (multiple - remainder);
  }

  return value;
}


int main(int argc, char** argv)
{

  int i, j;   
  
  // size of given matrix
  int height_x;
  int width_x;

  // size of output matrix
  int height_y;
  int width_y;

  // size of filter
  int height_F;
  int width_F;
  

  if (argc < 2)
  {
    std::cout << ">>>  Scheme was not given!" << std::endl;
    exit(1);
  }

  if ((strcmp(argv[1],"jacobi") != 0) & (strcmp(argv[1], "sor") !=0))
  {
    std::cout << ">>>  Incorrect scheme!" << std::endl;
    exit(1);
  }


/*________________________READ DATA________________________*/
#ifdef READ_FILE
  std::string xfname = "../data/x.txt";    
  std::ifstream xFile(xfname);
  
  if (!xFile.is_open())
  {
    std::cout << ">>> Problem in input file(s)" << std::endl;
    exit(1);
  }
  
  xFile >> height_x;
  xFile >> width_x;
#endif

  std::string dir = "../data/";
  std::string Ffname; 
  Ffname = dir + "F.txt" ; //dir +  argv[1] + ".txt";
  std::ifstream FFile(Ffname);

  if (!FFile.is_open())
  {
    std::cout << ">>> Problem in input file(s)" << std::endl;
    exit(1);
  }

  FFile >> height_F;
  FFile >> width_F;

#ifdef READ_FILE
  xFile >> height_x;
  xFile >> width_x;
#else
  std::cout << "Enter height and width of domain" << std::endl;
  std::cin >> height_x >> width_x;
#endif

  height_y = height_x;
  width_y = width_x;

   
  float* x = new float [height_x * width_x];
  float* F = new float [height_F * width_F];
  float* y = new float [height_y * width_y];  
  
  // Reading Data from file or shell
  for (i = 0; i < height_x * width_x; ++i)
  {
#ifdef READ_FILE
    xFile >> *(x + i);
#else
    *(x + i) = 1;
#endif
  }

  std::cout << "x(" << height_x << "*" << width_x << ")" << std::endl;
  for (i = 0; i < height_F * width_F; ++i)
  {
    FFile >> *(F + i);
  }

  if (x == NULL || F == NULL)
  {
    std::cout << " Matrix A or B is NULL" << std::endl;
    exit(0);
  }

  std::cout << "Filter(" << height_F << "*" << width_F << ")" << std::endl;
  for (i=0; i < height_F; ++i)
  {
    for (j=0; j < width_F; ++j)
    {
      std::cout << *(F + i * width_F + j) << "\t";
    }
    std::cout << std::endl;
  }


/*______________________SETUP DEVICES__________________________*/
  // get all platforms (drivers)
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);
    
  assert(platforms.size() > 0);

  std::cout << "_______________DEVICE INFO_______________" << std::endl;  
  cl::Platform myPlatform = platforms[0];
  std::cout << "Using platform: " << myPlatform.getInfo<CL_PLATFORM_NAME>() << std::endl;
     
  // get default device of the default platform
  std::vector<cl::Device> devices;
  myPlatform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
  
  assert(devices.size() > 0);
   
  cl::Device myDevice = devices[0];
  std::cout << "Using device: " << myDevice.getInfo<CL_DEVICE_NAME>() << std::endl;
  std::vector<::size_t> maxWorkItems;
  maxWorkItems = myDevice.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
  std::cout << "Max. Work Item Sizes: " << maxWorkItems[0] << "*" << maxWorkItems[1] << "*" << maxWorkItems[2] << std::endl;
  std::cout << "Max. Work Group Size: " << myDevice.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() << std::endl;
  std::cout << "Global Memory Size: " << myDevice.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>()
   / 1024/1024/1024 << " GB" << std::endl;
  std::cout << "Local Memory Size: " << myDevice.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>() 
    / 1024 << " KB" << std::endl;
  std::cout << "Constant Memory Size: " << myDevice.getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>() 
    / 1024 << " KB" << std::endl;
  std::cout << "Number of Compute Unit: " << myDevice.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << std::endl;

  std::cout << "______________________________________\n" << std::endl;  
  
  // file contains Kernel
#ifdef CL_READ4 
  std::ifstream kernelFile("convolution_read4.cl");
#else
  std::ifstream kernelFile("convolution.cl");
#endif
  std::string src(std::istreambuf_iterator<char>(kernelFile), (std::istreambuf_iterator<char>()));
  
  cl::Program::Sources sources(1,std::make_pair(src.c_str(),src.length() + 1));
  
  cl::Context context(myDevice);
  cl::Program program(context, sources);

#ifdef CL_PROFILING
  // create queue to which we will push commands for the device.
  cl::CommandQueue queue(context, myDevice, CL_QUEUE_PROFILING_ENABLE);
#else
  cl::CommandQueue queue(context, myDevice);
#endif

  // Build the program  
  std::cout << "===> Start building program" << std::endl;
  try
  {
    program.build();
    std::cout << "   ---> Build Successfully " << std::endl;
  } catch(cl::Error& error)
  {
    std::cout << ">>> Problem in building program " << std::endl;
    std::cout << "   ---> " << getErrorString(error) << std::endl;
    std::cout << "   ---> " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(myDevice) << std::endl;
    exit(0);
  }


  // run the kernel
  std::cout << "===> Start creating kernel" << std::endl;
  cl::Kernel kernel;
  try
  {
#ifdef CL_READ4
     kernel = cl::Kernel(program, "convolution_read4");
#else
     kernel = cl::Kernel(program, "convolution");
#endif

  }catch (cl::Error& error)
  {
    std::cout << ">>> Problem in kernel  " << std::endl;
    std::cout << "   ---> " << getErrorString(error) << std::endl;
    exit(0);
  }


/*________________________SETUP DEVICE MEMORY________________________*/
  // Round up the matrix x to be the multiplier of WGX in column size
  int deviceWidth = roundUp(width_x, WGX);
  int deviceHeight = height_x;
  
  int deviceDataSize = deviceWidth * deviceHeight * sizeof(float);

  int filterRadius = (int) height_F/2;
  int padding = filterRadius * 2;

  std::cout << "===> Start creating buffer" << std::endl;
  cl::Buffer buffer_x;
  cl::Buffer buffer_F;
  cl::Buffer buffer_y;
  
  // create buffers on the device
  try
  {
     buffer_x = cl::Buffer(context, CL_MEM_READ_ONLY,  deviceDataSize);
     buffer_F = cl::Buffer(context, CL_MEM_READ_ONLY,  height_F * width_F * sizeof(float));
     buffer_y = cl::Buffer(context, CL_MEM_WRITE_ONLY, deviceDataSize);
  }catch (cl::Error& error)
  {
    std::cout << ">>> Problem in creating buffer(s) " << std::endl;
    std::cout << "   --->" << getErrorString(error) << std::endl;
    exit(0);
  }
  
  // write arrays A and B to the device
  cl::size_t<3> buffer_origin;
  buffer_origin[0] = 0;
  buffer_origin[1] = 0;
  buffer_origin[2] = 0;
  
  cl::size_t<3> host_origin;
  host_origin[0] = 0;
  host_origin[1] = 0;
  host_origin[2] = 0;
  
  cl::size_t<3> region;
  region[0] = (size_t)(deviceWidth * sizeof(float));
  region[1] = (size_t)height_x;
  region[2] = 1;
  
  std::cout << "===> Start writing data to device" << std::endl;
  try
  {
    queue.enqueueWriteBufferRect(buffer_x, CL_TRUE, buffer_origin, host_origin, region                      ,deviceWidth * sizeof(float), 0, deviceWidth * sizeof(float), 0, x);
  
    queue.enqueueWriteBuffer(buffer_F, CL_TRUE, 0, height_F * width_F * sizeof(float), F);
  } catch (cl::Error& error)
  {
    std::cout << ">>> Problem in writing data from Host to Device: " << std::endl;
    std::cout << "   --->" << getErrorString(error) << std::endl;
    exit(0);
  }

  // Specifying size of global and local work groups
  int wgWidth  = WGX;
  int wgHeight = WGY;

  int maxWorkgroupX = roundUp(width_x  - padding, wgWidth);
  int maxWorkgroupY = roundUp(height_x - padding, wgHeight);
  
  size_t localws[3]  = {(size_t)wgWidth,       (size_t)wgHeight,      1};
  size_t globalws[3] = {(size_t)maxWorkgroupX, (size_t)maxWorkgroupY, 1};

  std::cout << "maxWokGroup " << maxWorkgroupX << "  " << maxWorkgroupY <<  "   " << deviceWidth << std::endl;
  
  // amount of cached local data is workGroupSize + padding
#ifdef CL_READ4
  int localWidth = roundUp(localws[0] + padding, 4);
#else
  int localWidth = localws[0] + padding;
#endif

  int localHeight = localws[1] + padding;

  std::cout << "localWidth " << localWidth << "  " << localHeight <<std::endl;
  // size of local memory in GPU for dynamic memory allocation
  size_t localMemSize = (localWidth * localHeight * sizeof(float));


  // Set the passing arguments of the kernel
  kernel.setArg(0, buffer_x);
  kernel.setArg(1, buffer_F);
  kernel.setArg(2, buffer_y);
  kernel.setArg(3, deviceHeight);
  kernel.setArg(4, deviceWidth);
  kernel.setArg(5, width_F);
  kernel.setArg(6, localMemSize, NULL);
  kernel.setArg(7, localHeight);
  kernel.setArg(8, localWidth);
  
#ifndef CL_PROFILING 
  auto t1 = std::chrono::high_resolution_clock::now();
#endif
 
  // Event created to fetch data profilng from the kernel 
  cl::Event event;
  
  // Set NDRange of the global and local size
  
  cl::NDRange globalRange = cl::NDRange(globalws[0], globalws[1], globalws[2]);
  cl::NDRange localRange  = cl::NDRange(localws[0],  localws[1],  localws[2]);
  
  std::cout << "localws " << localws[0] << "  " << localws[1] <<std::endl;
  std::cout << "globalws " << globalws[0] << "  " << globalws[1] <<std::endl;
  std::cout << "===> Start running kernel" << std::endl;
  try
  {
    queue.enqueueNDRangeKernel(kernel, cl::NullRange, globalRange,
                               localRange , NULL, &event);
  }catch (cl::Error& error)
  {
    std::cout << ">>> Problem in enqueue " << std::endl;
    std::cout << "   --->" << getErrorString(error) << std::endl;
    exit(0);
  }

  std::cout << "===> Kernel finished running" << std::endl;
  try
  {
    queue.finish();
  }catch (cl::Error& error)
  {
    std::cout << ">>> Problem in finishing " << std::endl;
    std::cout << "   --->" << getErrorString(error) << std::endl;
    exit(0);
  }

#ifndef CL_PROFILING
  auto t2 = std::chrono::high_resolution_clock::now();
#endif

  // read result y from the device to array y
  buffer_origin[0] = 0; //(size_t)(filterRadius * sizeof(float));
  buffer_origin[1] = 0; //(size_t)filterRadius;
  buffer_origin[2] = 0;
  
  host_origin[0] = 0; //(size_t)(filterRadius * sizeof(float));
  host_origin[1] = 0; //(size_t)filterRadius;
  host_origin[2] = 0;

  // region of x
  region[0] = (size_t)((width_x) * sizeof(float));
  region[1] = (size_t)(height_x);
  region[2] = 1;


  std::cout << "===> Start reading data from device" << std::endl;
  try
  {
    queue.enqueueReadBufferRect(buffer_y, CL_TRUE, buffer_origin, host_origin,
        region, width_x * sizeof(float), 0, width_x * sizeof(float), 0, y);
  } catch (cl::Error& error)
  {
    std::cout << ">>> Problem reading buffer in device: "  << std::endl;
    std::cout << "   --->" << getErrorString(error) << std::endl;
    exit(0);
  }

  std::string yfname = "../data/y.txt";
  std::ofstream yFile(yfname);
 
  if (!yFile.is_open())
  {
    std::cout << "Problem in output file(s)" << std::endl;
    exit(1);
  }
  
  yFile << height_y << std::endl;
  yFile << width_y << std::endl;
  
  for(i = 0; i < height_y; ++i)
  {
    for(j = 0; j < width_y; ++j)
    {
      yFile <<  *(y + i * width_y + j) << std::endl;
      if (height_x <=10)
      std::cout << std::setw(8) << *(y + i * width_y + j)  << "  ";
    }
    if (height_x <=10)
      std::cout << std::endl;
  }
 
  long double seconds;
#ifdef CL_PROFILING
  cl_ulong time_start = event.getProfilingInfo<CL_PROFILING_COMMAND_START>();
  cl_ulong time_end =  event.getProfilingInfo<CL_PROFILING_COMMAND_END>();

  seconds = (time_end-time_start) * 1.0e-9;
#else
  seconds = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() * 1.0e-3;
#endif

  std::cout << "OpenCl Execution time= " << seconds << " seconds" << std::endl;
  std::cout << "==> Performance= " << (double)height_x * (double)width_x * 9.0 / seconds * 1e-9 << " GFlops" << std::endl;
  
  delete[] (x);
  delete[] (F);
  delete[] (y);

#ifdef READ_FILE
  xFile.close();
#endif

  FFile.close();
  yFile.close();
  
  return 0;
} 
