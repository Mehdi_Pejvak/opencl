

__kernel void convolution_read4(__global float4* x, __constant float* filter, 
                                __global float* y,
                                int rows, int cols, int filterWidth,
                                __local float* local_x, int localHeight, int localWidth)
{
   __local float4* local_x4;

   // padding for filter
   int filterRadius = (filterWidth / 2);
   int padding = filterRadius * 2;
  
   // size of work group in output
   int groupStartCol = get_group_id(0) * get_local_size(0) / 4;
   int groupStartRow = get_group_id(1) * get_local_size(1);

   // Flatten the localIds 0-255
   int localId = get_local_id(1) * get_local_size(0) + get_local_id(0);

   // local ID for work-items
   int localRow = (localId / (localWidth / 4));
   int localCol = (localId % (localWidth / 4));

   // global ID of each work-item
   int globalRow = groupStartRow + localRow;
   int globalCol = groupStartCol + localCol;
   
   // set the vector pointer to the correct scalar location in local memory
   //local_x4 = (__local float4*) &local_x[localRow * localWidth + localCol * 4];
   __local float4 tt;
   
   int pos =localRow * localWidth + localCol * 4;
   if ((globalRow < rows) && (globalCol < (cols / 4)) && (localRow < localHeight))
   {
      tt = x[globalRow * cols / 4 + globalCol]; 
      local_x[pos++] = tt.x; 
      local_x[pos++] = tt.y; 
      local_x[pos++]  = tt.z; 
      local_x[pos++] = tt.w; 
    // local_x4[0] = x[globalRow * cols / 4 + globalCol];
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   // Reassign local IDs based on each work 
   localCol = get_local_id(0);
   localRow = get_local_id(1);

   globalCol = get_group_id(0) * get_local_size(0) + localCol;
   globalRow = get_group_id(1) * get_local_size(1) + localRow;

   // perform convolution
   if ((globalRow < (rows - padding)) && (globalCol < (cols - padding)))
   {
     
     // Calculate right hand-side
     float sum = 0.0;

     int filterIdx = 0;

     for (int i = localRow; i < localRow + filterWidth; ++i)
     {
        int offset = i * localWidth + localCol;
        sum += local_x[offset++] * filter[filterIdx++];
        sum += local_x[offset++] * filter[filterIdx++];
        sum += local_x[offset++] * filter[filterIdx++];
     }
     *(y + (globalRow + filterRadius) * cols + (globalCol + filterRadius)) = sum;
    }
}

