

/*
 * @name   helloWorld.cc
 * @info   The code developed to print "Hello World"
 *         on the GPU by using OpenCL library
 * @author Mehdi
*/


#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>
#include <fstream>
#include <iostream>
#include <cassert>
#include <exception>

int main()
{
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);

  assert(platforms.size() > 0);

  auto myPlatform = platforms.front();
  std::cout << "Using platform: " << myPlatform.getInfo<CL_PLATFORM_NAME>() << std::endl;
  
  std::vector<cl::Device> devices;
  myPlatform.getDevices(CL_DEVICE_TYPE_GPU, &devices);

  auto myDevice = devices.front();
  std::cout<< "Using device: "<< myDevice.getInfo<CL_DEVICE_NAME>() << std::endl;

  std::ifstream helloworldfile("helloWorldKernel.cl");
  std::string src(std::istreambuf_iterator<char>(helloworldfile), (std::istreambuf_iterator<char>()));

  cl::Program::Sources source(1,std::make_pair(src.c_str(), src.length() + 1));

  cl::Context context(myDevice);
  cl::Program program(context,source);

  cl::CommandQueue queue(context,myDevice);
  try
  {  
     program.build("-cl-std=CL1.2");
  } catch(cl::Error& e)
  {
    std::cout << ">>> Error in building, " << e.what() << std::endl;
    std::cout << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(myDevice) << std::endl;
  }

  int err;
  int szChar = 16;
  char  buf[szChar];

  cl::Buffer memBuf(context, CL_MEM_READ_WRITE, sizeof(char) * szChar);

  cl::Kernel kernel(program, "helloWorld", &err);

  if(err != CL_SUCCESS)
  {
    std::cout<<">>> Error in creating kernel, error: "<< err << std::endl;
    exit(1);
  } 
 

  kernel.setArg(0,memBuf);

  queue.enqueueTask(kernel);

  err =  queue.enqueueReadBuffer(memBuf,CL_TRUE, 0, sizeof(buf), buf);
  
  if(err != CL_SUCCESS)
  {
    std::cout<<">>> Error in reading from device, error: "<< err << std::endl;
    exit(1);
  }

  std::cout << buf;

  return 0;
}
