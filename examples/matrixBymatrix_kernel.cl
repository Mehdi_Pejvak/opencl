


__kernel void matrixBymatrix(__global double* A, __global double* B, __global double* C, int mA, int nA , int mB, int nB) 
{
    // Get the index of the current element
    int myDim = get_global_id(0);
     
    int nC = nB;

    __local double A_local[BLOCK_SIZE];
    for (int i=0; i < nA; ++i)
    {
      A_local[i] = *(A + myDim * nA + i);
    }

    double temp = 0;
    for(int i = 0; i < nB; ++i)
    {
      temp = 0;
      for (int j = 0; j < nA; ++j)
      {
        //  *(C + myDim * nC + i) += *(A + myDim * nA + j) * *(B + j * nB + i);
//          temp += *(A + myDim * nA + j) * *(B + j * nB + i);
          temp += A_local[j] * *(B + j * nB + i);
      }
      *(C + myDim * nC + i) = temp;
    }
} 
