
/*
 * @name   BCG.cc
 * @info   The code implements Biconjugate Gradient
 *         on the GPU by using OpenCL library to solve the 
 *         given system of equation Ax=b.
 * @author Mehdi
*/

#ifndef
  #define __CL_ENABLE_EXCEPTIONS
#endif

#include <iostream>
#include <CL/cl.hpp>
#include <cassert>
#include <fstream>
#include <iostream>
#include <exception>

int main()
{

  //get all platforms (drivers)
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);
    
  assert(platforms.size() > 0);
    
  cl::Platform myPlatform = platforms[0];
  std::cout << ">> Using platform: " << myPlatform.getInfo<CL_PLATFORM_NAME>() << std::endl;
     
  // get default device of the default platform
  std::vector<cl::Device> devices;
  myPlatform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
  
  assert(devices.size() > 0);
  
  cl::Device myDevice = devices[0];
  std::cout << ">> Using device: " << myDevice.getInfo<CL_DEVICE_NAME>() << std::endl;
     
  std::ifstream matBymatFile("BCG_kernel.cl" );
  std::string src(std::istreambuf_iterator<char>(matBymatFile), (std::istreambuf_iterator<char>()));
  
  cl::Program::Sources sources(1,std::make_pair(src.c_str(),src.length() + 1));
  
  cl::Context context(myDevice);
  cl::Program program(context, sources);

  // create queue to which we will push commands for the device.
  cl::CommandQueue queue(context,myDevice);
  

  int i;   
  int mA;
  int nA;
  int mx;
  int mb;
  
  // A and B are the input matrices.
  // Two first rows of the file A.txt and B.txt are number of row and column
  // of matrices A and B. Matrices was written in lexogeraphcal order
 
  std::string Afname = "../data/A.txt";    
  std::ifstream AFile(Afname);

  std::string bfname = "../data/b.txt";
  std::ifstream bFile(Bfname);
 

  if (!bFile.is_open() || !AFile.is_open())
  {
    std::cout << ">>> Problem in input file(s)" << std::endl;
    exit(1);
  }

  AFile >> mA;
  AFile >> nA;

  bFile >> mb;

  int mx = nA;

  std::cout << "A(" << mA << "*" << nA << ")" << std::endl;
  std::cout << "b(" << mb << ")" << std::endl;

  if ( mA != mb )
  {
    std::cout << ">>> Matrix A and vector b are not compatible!" << std::endl;
    exit(1);
  }

  // create buffers on the device
  cl::Buffer buffer_A(context, CL_MEM_READ_WRITE, mA * nA * sizeof(double));
  cl::Buffer buffer_b(context, CL_MEM_READ_WRITE, mb * sizeof(double));
  cl::Buffer buffer_x(context, CL_MEM_READ_WRITE, mx * sizeof(double));
     
  double* A = new double [mA * nA];
  double* b = new double [mb];
  double* x = new double [mx];  
  
  for (i = 0; i < mA * nA; ++i)
  {
    AFile >> *(A + i);
  }

  for (i = 0; i < mb ; ++i)
  {
    bFile >> *(b + i);
  }

  // write arrays A and B to the device
  try
  {
    queue.enqueueWriteBuffer(buffer_A, CL_TRUE, 0, mA * nA * sizeof(double), A);
    queue.enqueueWriteBuffer(buffer_b, CL_TRUE, 0, mb * sizeof(double), b);
  } catch(cl::Error& error)
  {
    std::cout << ">>> Error in writing data from Host to Device: " << error.what() << std::endl;
  } 
  

  // Build the program 
  try
  {
    err = program.build();
  } catch (cl::Error& error)
  {
    std::cout << ">>> Error in  building the program: " << error.what() << std::endl;
    std::cout << " build result: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(myDevice) << std::endl;
  }
  
 

  // run the kernel
  cl::Kernel kernel(program, "matrixBymatrix", &err);
  
  if (err != CL_SUCCESS)
  {
    std::cout << ">>> Problem in kernel, code: " << err << std::endl;
    exit(0);
  }

  // Set the passing arguments of the kernel
  kernel.setArg(0, buffer_A);
  kernel.setArg(1, buffer_b);
  kernel.setArg(2, buffer_x);
  kernel.setArg(3, mA);
  kernel.setArg(4, nA);
  kernel.setArg(5, mb);
  kernel.setArg(6, n);
  
  try
  {
    queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(mA), cl::NullRange);
  } catch (cl::Error& error)
  {
    std::cout << ">>> Problem in enqueue, code: " << error.what() << std::endl;
    exit(0);
  }
  
  queue.finish();
  
  // read result C from the device to array C
  try
  {
    queue.enqueueReadBuffer(buffer_x, CL_TRUE, 0, mx * sizeof(double), C);
  } catch (cl::Error& error))
  {
    std::cout << ">>> Problem reading buffer in device, code: " << error.what() << std::endl;
    exit(0);
  }

  std::string Cfname = "../data/C.txt";
  std::ofstream CFile(Cfname);
 
  if (!CFile.is_open())
  {
    std::cout << "Problem in output file(s)" << std::endl;
    exit(1);
  }
  
  std::cout <<"C=  " << std::endl ;
 
  CFile << mC << std::endl;
  CFile << nC << std::endl;
  
  for(i = 0; i < mC; ++i)
  {
    std::cout << "|";
  
    for(int j = 0; j < nC; ++j)
    {
      std::cout <<  *(C + i * nC + j);
      CFile <<  *(C + i * nC + j) << std::endl;
    }
    
    std::cout << "|" << std::endl;
  }
  
  delete[] (A);
  delete[] (B);
  delete[] (C);
  AFile.close();
  BFile.close();
  CFile.close();
  
  return 0;
} 
