
#include <CL/cl.hpp>
#include <cassert>
#include <iostream>

int main()
{
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);

  assert(platforms.size() > 0);

  auto platform = platforms.front();

  std::cout << "Using platform: " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;
  
  std::vector<cl::Device> devices;
  platform.getDevices(CL_DEVICE_TYPE_ALL,&devices);

  assert(devices.size() > 0);

  cl::Device device = devices.front();
  std::cout << "Using device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
  std::cout << "device vendor: " << device.getInfo<CL_DEVICE_VENDOR>() << std::endl;
  std::cout << "Device version: " << device.getInfo<CL_DEVICE_VERSION>() << std::endl;


  device = devices[1];
  std::cout << "Using device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl;
  std::cout << "device vendor: " << device.getInfo<CL_DEVICE_VENDOR>() << std::endl;
  std::cout << "Device version: " << device.getInfo<CL_DEVICE_VERSION>() << std::endl;

}
