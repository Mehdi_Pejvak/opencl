

/**
 * @name   poisonSolv.cc
 * @info   The code implements solve the poison equation by iterative method.
 *         matrix 'x' is the matrix contains values of vertices in grids.
 *         matrix 'F' is filter data which implements given scheme on matrix x.
 *         matrix 'A' contains the coefficients of descritised Poison equation.
 *
 * @author Mehdi
 * @date   04.01.2020
*/

#define WGX 16
#define WGY 16
#define MULTIGRID
#define SOLVE


#include "misc.hpp"


int main(int argc, char** argv)
{
  int i;
  int n = 2000;          // number of step in internal domain
  int filterWidth = 3;
  double x_length = 1.0; // length of domain in x and y direction
  int num_itr = 2000;      // smoothing step in each multigrid step
  int num_step = 2;      // number of step in multigrid 
  double res_2 = 0;      // norm of error
  double res_inf = 0;    // inf norm of error
  double b_2 = 0;        // norm of right-hand side
  long double Seconds;   // execuation time

  if (argc < 2)
  { 
    std::cout << "Not enought input!" << std::endl;
    exit(0);
  }
 
  std::string scheme = argv[1];  // the scheme used to solve system of equation (Jacobi or SOR)
  std::string yfname = "../data/y.txt";
  int width_x[num_step];
  int height_x[num_step];
  int width_y[num_step];
  int height_y[num_step];
  float h[num_step];
  int deviceWidth[num_step];
  int deviceHeight[num_step];  
  int deviceDataSize[num_step];
  cl::NDRange globalRange[num_step];
  cl::NDRange localRange[num_step]; 
  cl::Event event;  // Event created to fetch data profilng from the kernel 
  int maxWorkgroupX[num_step];
  int maxWorkgroupY[num_step];

  int filterRadius = (int) filterWidth / 2;
  int padding      = filterRadius * 2;
  
  //________________________Reading Data_________________________//  
  // number of nodes in the domaini in h
  for (i = 0; i < num_step; ++i)
  {
    set_domain_length(n, n, height_x[i], width_x[i], i);
    set_domain_length(n, n, height_y[i], width_y[i], i);
  }
  
  double h0 = x_length / width_x[0];  // spacial step size
  for (i = 0; i < num_step; ++i)
  {
    h[i] = h0 * pow(2,i); 
  }
  
  // The folowing sizes are width and height of filters //
  // size of matrix A
  int height_A = filterWidth;
  int width_A  = filterWidth;
  
  // size of filter h
  int height_F = filterWidth;
  int width_F  = filterWidth;
  

  // size of Matrix A_2h
  int height_A2h = filterWidth;
  int width_A2h  = filterWidth;


  // size of filter 2h
  int height_F2h = filterWidth;
  int width_F2h  = filterWidth;


  // size of prolongation matrix
  int height_prol = filterWidth;
  int width_prol  = filterWidth;
  

  // size of restriction matrix
  int height_rest = filterWidth;
  int width_rest  = filterWidth;
   
  float* x             = new float [height_x[0]    * width_x[0]];
  float* b             = new float [height_x[0]    * width_x[0]];
  float* A             = new float [height_A       * width_A];
  float* F             = new float [height_F       * width_F];
  float* F2h           = new float [height_F2h     * width_F2h];
  float* A2h           = new float [height_A2h     * width_A2h];
  float* prol_matrix   = new float [height_prol    * width_prol];
  float* rest_matrix   = new float [height_rest    * width_rest];
  float* y             = new float [height_y[0]    * width_y[0]];  
  float* res           = new float [height_x[0]    * width_x[0]];  
  float* res2h         = new float [height_x[1]    * width_x[1]];
  float* err           = new float [height_x[0]    * width_x[0]];
  float* err2h         = new float [height_x[1]    * width_x[1]];  
  float* err2h_relaxed   = new float [height_x[1]    * width_x[1]];
  
  create_data(x, A, A2h, F, F2h, prol_matrix, rest_matrix, width_x[0], height_x[0], filterWidth, h[0], n, scheme);
  init_unknown(x,             height_x[0], width_x[0], 2);
  init_unknown(err,           height_x[0], width_x[0], 0);
  init_unknown(err2h,         height_x[1], width_x[1], 0);
  init_unknown(err2h_relaxed, height_x[1], width_x[1], 0);
  init_unknown(y,             height_x[0], width_x[0], 0);
  init_unknown(res,           height_x[0], width_x[0], 0);
  init_unknown(res2h,         height_x[1], width_x[1], 0);

  set_bndryCond(x, width_x[0], h[0]);
  set_bndryCond(y, width_x[0], h[0]);
  set_rightHandSide(b, h[0], width_x[0], height_x[0]);
  //print_data(y, height_x[0], width_x[0]);
  //print_data(x, height_x[0], width_x[0]);

  // Specifying device width, height, in steps of multigrid
  // Round up the matrix x to be the multiplier of WGX in column size
  deviceWidth[0]    = roundUp(width_x[0], WGX);
  deviceHeight[0]   = height_x[0];  
  deviceDataSize[0] = deviceWidth[0] * deviceHeight[0] * sizeof(float);
  
  deviceWidth[1]    = roundUp(width_x[1], WGX);
  deviceHeight[1]   = height_x[1];
  deviceDataSize[1] = deviceWidth[1] * deviceHeight[1] * sizeof(float);
  

  // Set up arguments used in enqueueWriteBufferRect
  cl::size_t<3> buffer_origin;
  buffer_origin[0] = 0;
  buffer_origin[1] = 0;
  buffer_origin[2] = 0;
  
  cl::size_t<3> host_origin;
  host_origin[0] = 0;
  host_origin[1] = 0;
  host_origin[2] = 0;
  
  cl::size_t<3> region;
  region[0] = (size_t)(deviceWidth[0] * sizeof(float));
  region[1] = (size_t)(height_x[0]);
  region[2] = 1;
  
  cl::size_t<3> region2h;
  region2h[0] = (size_t)(deviceWidth[1] * sizeof(float)); 
  region2h[1] = (size_t)(height_x[1]);
  region2h[2] = 1;
  
  // Specifying size of global and local work groups in h
  int wgWidth  = WGX;
  int wgHeight = WGY;
  
  maxWorkgroupX[0] = roundUp(width_x[0]  - padding, wgWidth);
  maxWorkgroupY[0] = roundUp(height_x[0] - padding, wgHeight);
  maxWorkgroupX[1] = roundUp(width_x[1]  - padding, wgWidth);
  maxWorkgroupY[1] = roundUp(height_x[1] - padding, wgHeight);

  size_t localws[3]  = {(size_t)wgWidth,       (size_t)wgHeight,      1};

  // amount of cached local data is workGroupSize + padding
#ifdef CL_READ4
  int localWidth = roundUp(localws[0] + padding, 4);
#else
  int localWidth = localws[0] + padding;
#endif

  int localHeight = localws[1] + padding;

  // size of local memory in GPU for dynamic memory allocation
  size_t localMemSize = (localWidth * localHeight * sizeof(float));
  
  std::cout << "______________MULTIGRID INFO_____________" << std::endl;
  std::cout << "Number of node= " << width_x[0] << " * " << height_x[0] << std::endl;
  std::cout << "MultiGrid Step= " << num_step << std::endl;
  std::cout << "Relaxation step= " << num_itr << std::endl << std::endl;
  std::cout << "Local size= " << localHeight << " * " << localWidth << std::endl;
  std::cout << "Max WGSize h= " << maxWorkgroupX[0] << " * " << maxWorkgroupY[0] << std::endl;
  std::cout << "Max WGSize 2h= " << maxWorkgroupX[1] << " * " << maxWorkgroupY[1] << std::endl;
  

#ifdef DEBUG
  std::cout << "==> Filters: " << std::endl;
  std::cout << "A(" << height_A << "*" << width_A << ")" << std::endl;
  print_data(A, width_A, height_A);

  std::cout << "A2h(" << height_A2h << "*" << width_A2h << ")" << std::endl;
  print_data(A2h, width_A2h, height_A2h);
  
  std::cout << "F(" << height_F << "*" << width_F << ")" << std::endl;
  print_data(F, width_F, height_F);

  std::cout << "F2h(" << height_F2h << "*" << width_F2h << ")" << std::endl;
  print_data(F2h, width_F2h, height_F2h);

  std::cout << "prol_matrix(" << height_prol << "*" << width_prol << ")" << std::endl;
  print_data(prol_matrix, width_prol, height_prol);

  std::cout << "rest_matrix(" << height_rest << "*" << width_rest << ")" << std::endl;
  print_data(rest_matrix, width_rest, height_rest);
#endif

//_________________________Reading Device Info____________________________//

  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);
    
  assert(platforms.size() > 0);
  cl::Platform myPlatform = platforms[0];

  // get default device of the default platform
  std::vector<cl::Device> devices;
  myPlatform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
  
  assert(devices.size() > 0);
   
  cl::Device myDevice = devices[0];
  cl_display_info(myPlatform, myDevice);
  cl::Context context(myDevice);
   
  // file contains Kernel 
  std::ifstream kernelFile("multigrid.cl");

  std::string src(std::istreambuf_iterator<char>(kernelFile), (std::istreambuf_iterator<char>()));
  
  cl::Program::Sources sources(1,std::make_pair(src.c_str(),src.length() + 1));
  cl::Program program(context, sources);
  
  // create queue to which we will push commands for the device.
#ifdef CL_PROFILING
  cl::CommandQueue queue(context, myDevice, CL_QUEUE_PROFILING_ENABLE);
#else
  cl::CommandQueue queue(context, myDevice);
#endif

  std::cout << "==> Start creating buffer" << std::endl;
  cl::Buffer buffer_x;
  cl::Buffer buffer_b;
  cl::Buffer buffer_A;  
  cl::Buffer buffer_A2h;
  cl::Buffer buffer_F;      // Filter implements iterative scheme in h
  cl::Buffer buffer_F2h;    // Filter implements iterative scheme in 2h
  cl::Buffer buffer_prol;   // Filter implements prolongation
  cl::Buffer buffer_rest;   // Filter implements restriction
  cl::Buffer buffer_res;    // store residual after implementing iterative scheme in h
  cl::Buffer buffer_res2h;  // store residual in 2h by transfering from h to 2h
  cl::Buffer buffer_err;    // store error in h
  cl::Buffer buffer_err2h;    // store error in h
  cl::Buffer buffer_err2h_relaxed;  // store error after implementing iterative scheme in 2h
  cl::Buffer buffer_y;      // result matrix
  
  try
  {
     buffer_x           = cl::Buffer(context, CL_MEM_READ_WRITE,  deviceDataSize[0]);
     buffer_b           = cl::Buffer(context, CL_MEM_READ_WRITE,  deviceDataSize[0]);
     buffer_F           = cl::Buffer(context, CL_MEM_READ_ONLY,  height_F    * width_F    * sizeof(float));
     buffer_F2h         = cl::Buffer(context, CL_MEM_READ_ONLY,  height_F2h  * width_F2h  * sizeof(float));
     buffer_A           = cl::Buffer(context, CL_MEM_READ_ONLY,  height_A    * width_A    * sizeof(float));
     buffer_A2h         = cl::Buffer(context, CL_MEM_READ_ONLY,  height_A2h  * width_A2h  * sizeof(float));
     buffer_prol        = cl::Buffer(context, CL_MEM_READ_ONLY,  height_prol * width_prol * sizeof(float));
     buffer_rest        = cl::Buffer(context, CL_MEM_READ_ONLY,  height_rest * width_rest * sizeof(float));
     buffer_res         = cl::Buffer(context, CL_MEM_READ_WRITE, deviceDataSize[0]);
     buffer_res2h       = cl::Buffer(context, CL_MEM_READ_WRITE, deviceDataSize[1]);
     buffer_err         = cl::Buffer(context, CL_MEM_READ_WRITE, deviceDataSize[0]);
     buffer_err2h         = cl::Buffer(context, CL_MEM_READ_WRITE, deviceDataSize[1]);
     buffer_err2h_relaxed = cl::Buffer(context, CL_MEM_READ_WRITE, deviceDataSize[1]);
     buffer_y           = cl::Buffer(context, CL_MEM_READ_WRITE, deviceDataSize[0]);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem in creating buffer(s) " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
	
  
  std::cout << "==> Start writing data to device" << std::endl;
  try
  {
    queue.enqueueWriteBufferRect(buffer_x, CL_TRUE, buffer_origin, host_origin, region,
                                 deviceWidth[0] * sizeof(float), 0, deviceWidth[0] * sizeof(float), 0, x);
    queue.enqueueWriteBufferRect(buffer_b, CL_TRUE, buffer_origin, host_origin, region,
                                 deviceWidth[0] * sizeof(float), 0, deviceWidth[0] * sizeof(float), 0, b);
    queue.enqueueWriteBuffer(buffer_A,    CL_TRUE, 0, height_A    * width_A    * sizeof(float), A);
    queue.enqueueWriteBuffer(buffer_A2h,  CL_TRUE, 0, height_A2h  * width_A2h  * sizeof(float), A2h);
    queue.enqueueWriteBuffer(buffer_F,    CL_TRUE, 0, height_F    * width_F    * sizeof(float), F);
    queue.enqueueWriteBuffer(buffer_F2h,  CL_TRUE, 0, height_F2h  * width_F2h  * sizeof(float), F2h);
    queue.enqueueWriteBuffer(buffer_prol, CL_TRUE, 0, height_prol * width_prol * sizeof(float), prol_matrix);
    queue.enqueueWriteBuffer(buffer_rest, CL_TRUE, 0, height_rest * width_rest * sizeof(float), rest_matrix);
    queue.enqueueWriteBufferRect(buffer_res, CL_TRUE, buffer_origin, host_origin, region,
                                 deviceWidth[0] * sizeof(float), 0, deviceWidth[0] * sizeof(float), 0, res);
    queue.enqueueWriteBufferRect(buffer_res2h, CL_TRUE, buffer_origin, host_origin, region2h,
                                 deviceWidth[1] * sizeof(float), 0, deviceWidth[1] * sizeof(float), 0, res2h);
    queue.enqueueWriteBufferRect(buffer_err, CL_TRUE, buffer_origin, host_origin, region2h,
                                 deviceWidth[0] * sizeof(float), 0, deviceWidth[0] * sizeof(float), 0, err);
    queue.enqueueWriteBufferRect(buffer_err2h_relaxed, CL_TRUE, buffer_origin, host_origin, region2h,
                                 deviceWidth[1] * sizeof(float), 0, deviceWidth[1] * sizeof(float), 0, err2h);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem in writing data from Host to Device: " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  // Copy buffer x into buffer y
  try
  {
    queue.enqueueCopyBufferRect(buffer_x, buffer_y, buffer_origin, host_origin, region,
                   deviceWidth[0] * sizeof(float), 0, deviceWidth[0] * sizeof(float), 0);
    queue.enqueueCopyBufferRect(buffer_err2h, buffer_err2h_relaxed, buffer_origin, host_origin, region2h,
                   deviceWidth[1] * sizeof(float), 0, deviceWidth[1] * sizeof(float), 0);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem in copying data from Host to Device: " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  //_________________Build the program___________________
  std::cout << "==> Start building program" << std::endl;
#ifdef CL_BUILD_OPTION
  std::string buildOptions;
  { // create preprocessor defines for the kernel
     char buf[256]; 
     sprintf(buf,"-D h=%lf", (double)x_length / width_x[0]);
     buildOptions += std::string(buf);
  }

  try
  {
    program.build(buildOptions.c_str());
    std::cout << "  -> Build Successfully " << std::endl;
  } catch(cl::Error& error)
  {
    std::cout << "  -> Problem in building program: " << getErrorString(error) << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    std::cout << "  -> " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(myDevice) << std::endl;
    exit(0);
  }
#else
  try
  {
    program.build("-cl-std=CL2.0");
    std::cout << "  -> Build Successfully " << std::endl;
  } catch(cl::Error& error)
  {
    std::cout << "  -> Problem in building program " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    std::cout << "  -> " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(myDevice) << std::endl;
    exit(0);
  }
#endif

  // run the kernel
  std::cout << "==> Start creating kernel" << std::endl;
  cl::Kernel kernel_smooth;
  cl::Kernel kernel_res;
  cl::Kernel kernel_rest;
  cl::Kernel kernel_copy;
  cl::Kernel kernel_prol;
  cl::Kernel kernel_add;
  
  try
  {
    kernel_smooth = cl::Kernel(program, "iterative_scheme");
    kernel_res    = cl::Kernel(program, "residual");
    kernel_rest   = cl::Kernel(program, "restriction");
    kernel_copy   = cl::Kernel(program, "copy");
    kernel_prol   = cl::Kernel(program, "prolongation");
    kernel_add    = cl::Kernel(program, "add");
     
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in kernel  " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

#ifdef MULTIGRID
  std::cout << ">>> Solve With MultiGrid Porcesses  <<<" << std::endl;
#endif

#ifndef MULTIGRID
  std::cout << ">>> Solve With " << scheme << " Algorithm  <<<" << std::endl;
#endif


#ifndef CL_PROFILING 
  auto t1 = std::chrono::high_resolution_clock::now();
#endif
 
#ifdef SOLVE
//___________________Smoothing in H______________________ //
  std::cout << "==> Start smoothing in h" << std::endl;
  // Specifying size of global work groups in h

  size_t globalws[3] = {(size_t)maxWorkgroupX[0], (size_t)maxWorkgroupY[0], 1};

  int argCount = 0;
  try
  {
    kernel_smooth.setArg(argCount++, buffer_x);
    kernel_smooth.setArg(argCount++, deviceHeight[0]);
    kernel_smooth.setArg(argCount++, width_x[0]);
    kernel_smooth.setArg(argCount++, buffer_F);
    kernel_smooth.setArg(argCount++, width_F);
    kernel_smooth.setArg(argCount++, buffer_b);
    kernel_smooth.setArg(argCount++, buffer_y);
    kernel_smooth.setArg(argCount++, h[0]);
    kernel_smooth.setArg(argCount++, localMemSize, NULL);
    kernel_smooth.setArg(argCount++, localHeight);
    kernel_smooth.setArg(argCount++, localWidth);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem in setting the argument of kernel smooth" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  // Set NDRange of the global and local size
  globalRange[0] = cl::NDRange(globalws[0], globalws[1], globalws[2]);
  localRange[0]  = cl::NDRange(localws[0],  localws[1],  localws[2] );
 
  for (int i = 0; i < num_itr; ++i)
  {
    try
    {
      queue.enqueueNDRangeKernel(kernel_smooth, cl::NullRange, globalRange[0], localRange[0], NULL, NULL);
    }catch (cl::Error& error)
    {
      std::cout << "  -> Problem in enqueue kernel smooth" << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }

    try
    {
      queue.finish();
    }catch (cl::Error& error)
    {
      std::cout << "  -> Problem in finishing kernel smooth" << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }
    
    try
    {
      queue.enqueueCopyBufferRect(buffer_y, buffer_x, buffer_origin, host_origin, region,
        deviceWidth[0] * sizeof(float), 0, deviceWidth[0] * sizeof(float), 0, NULL, &event);
    } catch (cl::Error& error)
    {
      std::cout << "  -> Problem in copying buffer x to y" << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }
    event.wait();
   }
  
//___________________Residual in H________________________ //
  std::cout << "==> Calculation of residual in h" << std::endl;
  argCount = 0;
  try
  {
    kernel_res.setArg(argCount++, buffer_x);
    kernel_res.setArg(argCount++, deviceHeight[0]);
    kernel_res.setArg(argCount++, width_x[0]);
    kernel_res.setArg(argCount++, buffer_A);
    kernel_res.setArg(argCount++, width_A);
    kernel_res.setArg(argCount++, buffer_b);
    kernel_res.setArg(argCount++, buffer_res);
    kernel_res.setArg(argCount++, h[0]);
    kernel_res.setArg(argCount++, localMemSize, NULL);
    kernel_res.setArg(argCount++, localHeight);
    kernel_res.setArg(argCount++, localWidth);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem in setting the argument of kernel residual" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.enqueueNDRangeKernel(kernel_res, cl::NullRange, globalRange[0], localRange[0], NULL, &event);
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in enqueue kernel residual" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in finishing kernel residual" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

#endif

#ifdef MULTIGRID
 
//_____________________Restriction_______________________//
  std::cout << "==> Restriction" << std::endl;
  argCount = 0;
  try
  {
    kernel_rest.setArg(argCount++, buffer_res);
    kernel_rest.setArg(argCount++, deviceHeight[0]);
    kernel_rest.setArg(argCount++, width_x[0]);
    kernel_rest.setArg(argCount++, buffer_rest);
    kernel_rest.setArg(argCount++, width_F);
    kernel_rest.setArg(argCount++, buffer_res2h);
    kernel_rest.setArg(argCount++, localMemSize, NULL);
    kernel_rest.setArg(argCount++, localHeight);
    kernel_rest.setArg(argCount++, localWidth);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem in setting the argument of kernel restriction" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  try
  {
    queue.enqueueNDRangeKernel(kernel_rest, cl::NullRange, globalRange[0], localRange[0], NULL, &event);
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in enqueue kernel restriction" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in finishing restriction" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

//_____________________Smoothing of residual in 2H______________________//
  std::cout << "==> Smoothing residual in 2h" << std::endl;
  // Specifying size of global and local work groups in 2h
  globalws[0] = (size_t)maxWorkgroupX[1]; globalws[1] = (size_t)maxWorkgroupY[1]; globalws[2] = 1;
  
  // Set NDRange of the global and local size
  globalRange[1] = cl::NDRange(globalws[0], globalws[1], globalws[2]);

 // Set the passing arguments of the kernel to do smoothing
  argCount = 0;
  try
  {
    kernel_smooth.setArg(argCount++, buffer_err);
    kernel_smooth.setArg(argCount++, deviceHeight[1]);
    kernel_smooth.setArg(argCount++, width_x[1]);
    kernel_smooth.setArg(argCount++, buffer_F2h);
    kernel_smooth.setArg(argCount++, width_F2h);
    kernel_smooth.setArg(argCount++, buffer_res2h);
    kernel_smooth.setArg(argCount++, buffer_err2h_relaxed);
    kernel_smooth.setArg(argCount++, h[1]);
    kernel_smooth.setArg(argCount++, localMemSize, NULL);
    kernel_smooth.setArg(argCount++, localHeight);
    kernel_smooth.setArg(argCount++, localWidth);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem in setting the argument of kernel smooth" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  
  for (int i = 0; i < num_itr; ++i)
  {
    try
    {
      queue.enqueueNDRangeKernel(kernel_smooth, cl::NullRange, globalRange[1], localRange[1], NULL, &event);
    }catch (cl::Error& error)
    {
      std::cout << "  -> Problem in enqueue kernel smooth" << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }

    try
    {
      queue.finish();
    }catch (cl::Error& error)
    {
      std::cout << "  -> Problem in finishing kernel smooth" << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }
    
    try
    {
      queue.enqueueCopyBufferRect(buffer_err2h_relaxed, buffer_err2h, buffer_origin, host_origin, region2h,
        deviceWidth[1] * sizeof(float), 0, deviceWidth[1] * sizeof(float), 0, NULL, &event);
    } catch (cl::Error& error)
    {
      std::cout << "  -> Problem in copying buffer res to res_relaxed " << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }
    event.wait();
 }

 
//_____________________Prolongation_______________________//
  std::cout << "==> Prolongation" << std::endl;
  
  argCount = 0;
  try
  {
    kernel_copy.setArg(argCount++, buffer_err2h);
    kernel_copy.setArg(argCount++, deviceHeight[1]);
    kernel_copy.setArg(argCount++, width_x[1]);
    kernel_copy.setArg(argCount++, buffer_err);
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in setting the argument of kernel copy" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  try
  {
    queue.enqueueNDRangeKernel(kernel_copy, cl::NullRange, globalRange[1], localRange[1], NULL, &event);
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in enqueue kernel copy" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in finishing kernel copy" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  

  argCount = 0;
  try
  {
    kernel_prol.setArg(argCount++, buffer_err);
    kernel_prol.setArg(argCount++, deviceHeight[0]);
    kernel_prol.setArg(argCount++, width_x[0]);
    kernel_prol.setArg(argCount++, buffer_prol);
    kernel_prol.setArg(argCount++, width_F);
    kernel_prol.setArg(argCount++, localMemSize, NULL);
    kernel_prol.setArg(argCount++, localHeight);
    kernel_prol.setArg(argCount++, localWidth);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem in setting the argument of kernel prolongation" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
  
  try
  {
    queue.enqueueNDRangeKernel(kernel_prol, cl::NullRange, globalRange[0], localRange[0], NULL, &event);
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in enqueue kernel prolongation" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in finishing kernel prolongation" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
 
  //_____________________Add error to vector x______________________//
  std::cout << "==> Add smoothed error to vector x" << std::endl;
  
  argCount = 0;
  try
  {
    kernel_add.setArg(argCount++, buffer_x);
    kernel_add.setArg(argCount++, deviceHeight[0]);
    kernel_add.setArg(argCount++, width_x[0]);
    kernel_add.setArg(argCount++, buffer_err);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem in setting the argument of kernel add" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.enqueueNDRangeKernel(kernel_add, cl::NullRange, globalRange[0], localRange[0], NULL, &event);
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in enqueue kernel add" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in finishing kernel add" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }


  //_____________________Smoothing in H______________________//
  std::cout << "==> Start smoothing in h" << std::endl;
  argCount = 0;
  try
  {
    kernel_smooth.setArg(argCount++, buffer_x);
    kernel_smooth.setArg(argCount++, deviceHeight[0]);
    kernel_smooth.setArg(argCount++, width_x[0]);
    kernel_smooth.setArg(argCount++, buffer_F);
    kernel_smooth.setArg(argCount++, width_F);
    kernel_smooth.setArg(argCount++, buffer_b);
    kernel_smooth.setArg(argCount++, buffer_y);
    kernel_smooth.setArg(argCount++, h[0]);
    kernel_smooth.setArg(argCount++, localMemSize, NULL);
    kernel_smooth.setArg(argCount++, localHeight);
    kernel_smooth.setArg(argCount++, localWidth);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem in setting the argument of kernel smooth" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

   for (int i = 0; i < num_itr; ++i)
  {
    try
    {
      queue.enqueueNDRangeKernel(kernel_smooth, cl::NullRange, globalRange[0], localRange[0], NULL, &event);
    }catch (cl::Error& error)
    {
      std::cout << "  -> Problem in enqueue kernel smooth" << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }


    try
    {
      queue.finish();
    }catch (cl::Error& error)
    {
      std::cout << "  -> Problem in finishing kernel smooth" << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }
    
    try
    {
      queue.enqueueCopyBufferRect(buffer_y, buffer_x, buffer_origin, host_origin, region,
                                 deviceWidth[0] * sizeof(float), 0, deviceWidth[0] * sizeof(float), 0, NULL,  &event);
    } catch (cl::Error& error)
    {
      std::cout << "  -> Problem in copying buffer x to y: " << std::endl;
      std::cout << "  -> " << getErrorString(error) << std::endl;
      exit(0);
    }
    event.wait();
 }

   
//___________________Residual in H________________________ //
  std::cout << "==> Calculation of residual in h" << std::endl;
  argCount = 0;
  try
  {
    kernel_res.setArg(argCount++, buffer_x);
    kernel_res.setArg(argCount++, deviceHeight[0]);
    kernel_res.setArg(argCount++, width_x[0]);
    kernel_res.setArg(argCount++, buffer_A);
    kernel_res.setArg(argCount++, width_A);
    kernel_res.setArg(argCount++, buffer_b);
    kernel_res.setArg(argCount++, buffer_res);
    kernel_res.setArg(argCount++, h[0]);
    kernel_res.setArg(argCount++, localMemSize, NULL);
    kernel_res.setArg(argCount++, localHeight);
    kernel_res.setArg(argCount++, localWidth);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem in setting the argument of kernel residual" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.enqueueNDRangeKernel(kernel_res, cl::NullRange, globalRange[0], localRange[0], NULL, &event);
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in enqueue kernel residual" << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (cl::Error& error)
  {
    std::cout << "  -> Problem in finishing " << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }
#endif


  //______________________Reading Result_______________________//
#ifndef CL_PROFILING
  auto t2 = std::chrono::high_resolution_clock::now();
#endif
  
  std::cout << "==> Start reading data from device" << std::endl;
  // read result y and residual from the device 
  buffer_origin[0] = 0; 
  buffer_origin[1] = 0;
  buffer_origin[2] = 0;
  
  host_origin[0] = 0; 
  host_origin[1] = 0; 
  host_origin[2] = 0;

  // region of x
  region[0] = (size_t)(width_x[0] * sizeof(float));
  region[1] = (size_t)(height_x[0]);
  region[2] = 1;
  
  try
  {
    queue.enqueueReadBufferRect(buffer_res, CL_TRUE, buffer_origin, host_origin,
        region, width_x[0] * sizeof(float), 0, width_x[0] * sizeof(float), 0, res);
    queue.enqueueReadBufferRect(buffer_x, CL_TRUE, buffer_origin, host_origin,
        region, width_x[0] * sizeof(float), 0, width_x[0] * sizeof(float), 0, x);
  } catch (cl::Error& error)
  {
    std::cout << "  -> Problem reading buffer in device: "  << std::endl;
    std::cout << "  -> " << getErrorString(error) << std::endl;
    exit(0);
  }

  // Calculation of norm of residual
  norm(res, height_x[0], width_x[0],res_2);
  norm_inf(res, height_x[0], width_x[0],res_inf);
  norm(b, height_x[0], width_x[0],b_2);
  
  if(n < 20)
  { 
    std::cout << "x:" << std::endl;
    print_data(x, height_x[0], width_x[0]);
    print_data(b, height_x[0], width_x[0]);
    print_data(res, height_x[0], width_x[0]);
  }

  // Write the result into a file 
  write_file(yfname, y, height_y[0], width_y[0]);

#ifdef CL_PROFILING
  cl_ulong time_start = event.getProfilingInfo<CL_PROFILING_COMMAND_START>();
  cl_ulong time_end   = event.getProfilingInfo<CL_PROFILING_COMMAND_END>();

  Seconds = (time_end - time_start) * 1.0e-9;
#else
  Seconds = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() * 1.0e-3; 
#endif
  
  std::cout << "\n\t_________________________________________________________"  << std::endl;
  std::cout << "\t|\t\t\tResult\t\t\t\t|" << std::endl;
  std::cout << "\t---------------------------------------------------------"  << std::endl;
  std::cout << "\t|" << std::setw(12) << "Exe. time (sec)|\t" << "||e||_2"  << "\t|\t" << "||e||_inf\t|" << std::endl;
  std::cout << "\t---------------------------------------------------------"  << std::endl;
  std::cout << "\t|" << std::setw(10) << Seconds << "\t|"       << std::setw(12) << res_2 / b_2 << "\t|\t" << std::setw(12) << res_inf << "\t|" << std::endl;
  std::cout << "\t---------------------------------------------------------\n"  << std::endl;
  
  delete[] (x);
  delete[] (b);
  delete[] (F);
  delete[] (A);
  delete[] (F2h);
  delete[] (A2h);
  delete[] (prol_matrix);
  delete[] (rest_matrix);
  delete[] (res);
  delete[] (res2h);
  delete[] (err);
  delete[] (err2h);
  delete[] (err2h_relaxed);
  delete[] (y);

  return 0;
} 
