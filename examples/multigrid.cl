

/*
 * The kernel used to implement iterative scheme
 */
__kernel void iterative_scheme(__global float* x, int height_x, int width_x, 
                               __constant float* filter, int filterWidth,
                               __global float* b,
                               __global float* y,
                               float h,
                               __local float* local_x, int localHeight, int localWidth)
{

   // padding for filter
   int filterRadius = (filterWidth / 2);
   int padding      = filterRadius * 2;
  
   // size of work group in output
   int groupStartCol = get_group_id(0) * get_local_size(0);
   int groupStartRow = get_group_id(1) * get_local_size(1);

   // local ID for work-items
   int localCol = get_local_id(0);
   int localRow = get_local_id(1);

   // global ID of each work-item
   int globalCol = groupStartCol + localCol;
   int globalRow = groupStartRow + localRow;

   /*__________________Impelement Filter___________________*/
   // step down rows
   for (int i = localRow; i < localHeight; i += get_local_size(1))
   {
      int currRow = groupStartRow + i;

      // step across columns
      for (int j = localCol; j < localWidth; j += get_local_size(0))
      {
         int currCol = groupStartCol + j;

         // perform copy to local memory
         if (currRow < height_x && currCol < width_x)
         {
            *(local_x + i * localWidth + j) = *(x + currRow * width_x + currCol);
         }
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   // perform convolution
   if ((globalRow < (height_x - padding)) && (globalCol < (width_x - padding)))
   {     
     // Calculate right hand-side
     
     float sum = b[(globalRow + filterRadius) * width_x + (globalCol + filterRadius)] * pow(h,2) * 0.25;

     int filterIdx = 0;
     //int offset;

     for (int i = localRow; i < localRow + filterWidth; ++i)
     {
        int offset= i * localWidth + localCol;
        sum += local_x[offset++] * filter[filterIdx++];
        sum += local_x[offset++] * filter[filterIdx++];
        sum += local_x[offset++] * filter[filterIdx++];
     }
     *(y + (globalRow + filterRadius) * width_x + (globalCol + filterRadius)) = sum;
   }
}
    
/*
 * The kernel calculates the residual of r = Ax - b
 */
__kernel void residual(__global float* x, int height_x, int width_x, 
		       __constant float* A, int width_A,
                       __global float* b,
		       __global float* res,
                       float h,
                       __local float* local_x, int localWidth, int localHeight)
{
   // padding for filter
   int filterRadius = (width_A / 2);
   int padding = filterRadius * 2;

   // size of work group in output
   int groupStartCol = get_group_id(0) * get_local_size(0);
   int groupStartRow = get_group_id(1) * get_local_size(1);

   // local ID for work-items
   int localCol = get_local_id(0);
   int localRow = get_local_id(1);

   // global ID of each work-item
   int globalCol = groupStartCol + localCol;
   int globalRow = groupStartRow + localRow;

   /* _________________Calculate the Error____________________*/

   // step down rows
   for (int i = localRow; i < localHeight; i += get_local_size(1))
   {
      int currRow = groupStartRow + i;

      // step across columns
      for (int j = localCol; j < localWidth; j += get_local_size(0))
      {
         int currCol = groupStartCol + j;

         // perform copy to local memory
         if (currRow < height_x && currCol < width_x)
         {
            *(local_x + i * localWidth + j) = *(x + currRow * width_x + currCol);
         }
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   // perform convolution
   if ((globalRow < (height_x - padding)) && (globalCol < (width_x - padding)))
   {
     // Calculate right hand-side
     float sum = - b[(globalRow + filterRadius) * width_x + (globalCol + filterRadius)] * pow(h,2);

     int filterIdx = 0;
     //int offset;

     for (int i = localRow; i < localRow + width_A; ++i)
     {
        int offset= i * localWidth + localCol;
        sum += local_x[offset++] * A[filterIdx++];
        sum += local_x[offset++] * A[filterIdx++];
        sum += local_x[offset++] * A[filterIdx++];
     }
     *(res + (globalRow + filterRadius) * width_x + (globalCol + filterRadius)) = sum;
   }
}



/*
 * The kernel implements restriction of multigrid (going down to coarse grid)
 */
__kernel void restriction(__global float* x, int height_x, int width_x, 
                          __constant float* A, int width_A,
                          __global float* y,
                          __local float* local_x, int localWidth, int localHeight)
{ 
   // padding for filter
   int filterRadius = (width_A / 2);
   int padding = filterRadius * 2;

   // size of work group in output
   int groupStartCol = get_group_id(0) * get_local_size(0);
   int groupStartRow = get_group_id(1) * get_local_size(1);

   // local ID for work-items
   int localCol = get_local_id(0);
   int localRow = get_local_id(1);

   // global ID of each work-item
   int globalCol = groupStartCol + localCol;
   int globalRow = groupStartRow + localRow;

   // step down rows
   for (int i = localRow; i < localHeight; i += get_local_size(1))
   {
      int currRow = groupStartRow + i;

      // step across columns
      for (int j = localCol; j < localWidth; j += get_local_size(0))
      {
         int currCol = groupStartCol + j;

         // perform copy to local memory
         if (currRow < height_x && currCol < width_x)
         {
            *(local_x + i * localWidth + j) = *(x + currRow * width_x + currCol);
         }
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   // perform convolution. The odd rows and columns are eliminated for y
   if ((globalRow < (height_x - padding)) && (globalCol < (width_x - padding)) && 
        ((globalRow + filterRadius )%2 == 0) && ((globalCol + filterRadius)%2 == 0))
   {
     // Calculate right hand-side
     float sum = 0;

     int filterIdx = 0;
     //int offset;

     for (int i = localRow; i < localRow + width_A; ++i)
     {
        int offset= i * localWidth + localCol;
        sum += local_x[offset++] * A[filterIdx++];
        sum += local_x[offset++] * A[filterIdx++];
        sum += local_x[offset++] * A[filterIdx++];
     }
     *(y +((globalRow + filterRadius) * width_x + globalCol + filterRadius) / 2) = sum;  
   }
}


/*
 * The kernel copies the data from coarse grid (x) to corresponding node in fine grid 
 * before performing prolongation (y)
 */
__kernel void  copy (__global float* x, int height_x, int width_x, 
                     __global float* y)
{
    int globalRow = get_global_id(1);
    int globalCol = get_global_id(0);
    
    *(y + (globalRow * width_x + globalCol) * 2) = *(x + globalRow * width_x + globalCol);
}

/*
 * The kernel implements prolongation of multigrid (going up to fine grid)
 * before performing prolongation, copy kernel should be done to copy 
 */
__kernel void prolongation (__global float* x,   int height_x, int width_x,  
                            __constant float* A, int width_A,
                            __local float* local_x, int localWidth, int localHeight)
{ 
   // padding for filter
   int filterRadius = (width_A / 2);
   int padding = filterRadius * 2;

   // size of work group in output
   int groupStartCol = get_group_id(0) * get_local_size(0);
   int groupStartRow = get_group_id(1) * get_local_size(1);

   // local ID for work-items
   int localCol = get_local_id(0);
   int localRow = get_local_id(1);

   // global ID of each work-item
   int globalCol = groupStartCol + localCol;
   int globalRow = groupStartRow + localRow;
 
   // step down rows
   for (int i = localRow; i < localHeight; i += get_local_size(1))
   {
      int currRow = groupStartRow + i;

      // step across columns
      for (int j = localCol; j < localWidth; j += get_local_size(0))
      {
         int currCol = groupStartCol + j;

         // perform copy to local memory
         if (currRow < height_x && currCol < width_x)
         {
            *(local_x + i * localWidth + j) = *(x + currRow * width_x + currCol);
         }
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   // perform convolution
   if ((globalRow < (height_x - padding)) && (globalCol < (width_x - padding)) 
        && ((globalRow + filterRadius)%2 != 0) && ((globalCol + filterRadius)%2 != 0))
   {
     // Calculate right hand-side
     float sum = 0;

     int filterIdx = 0;
     int offset;
     int count = 0;    // it count nonzero components of the A

     for (int i = localRow; i < localRow + width_A; ++i)
     {
        int offset= i * localWidth;
        for(int j = localCol; j < localCol + width_A; ++j)
        {
          if (A[filterIdx] != 0) count++;
          sum += local_x[offset+j] * A[filterIdx++];
        }
     }
     if (count == 0) count++;  // This is only to avoid division by zero, No impact on result count=0 -> sum=0 
     *(x + (globalRow + filterRadius) * width_x + (globalCol + filterRadius)) = sum / count;
   }
}


/*
 * The kernel add vector (y) to vector (x) 
 */
__kernel void  add (__global float* x, int height_x, int width_x, 
                     __global float* y)
{
    int globalRow = get_global_id(1);
    int globalCol = get_global_id(0);
    
    *(x + (globalRow * width_x + globalCol)) += *(y + globalRow * width_x + globalCol);
}
