
/*
 * @name   vectorAdd.cc
 * @info   The code developed to add two vectors
 *         on the GPU by using OpenCL library
 * @author Mehdi
*/


#include <iostream>
#include <CL/cl.hpp>
#include <cassert>
#include <fstream>
#include <time.h>
#include <cmath>
#define __CL_ENABLE_EXCEPTIONS

void randomInit(float *data, int size) 
{
    for (int i = 0; i < size; ++i)
        data[i] = rand() / (float)RAND_MAX;
}


int main()
{

  //get all platforms (drivers)
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);
    
  assert(platforms.size() > 0);
    
  cl::Platform myPlatform = platforms[0];
  std::cout << "Using platform: " << myPlatform.getInfo<CL_PLATFORM_NAME>() << std::endl;
     
  //get default device of the default platform
  std::vector<cl::Device> devices;
  myPlatform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
  
  assert(devices.size() > 0);
  
  cl::Device myDevice = devices[0];
  std::cout<< "Using device: "<< myDevice.getInfo<CL_DEVICE_NAME>() << std::endl;
     
  std::ifstream vectorAddFile("vector_add_kernel.cl" );
  std::string src(std::istreambuf_iterator<char>(vectorAddFile), (std::istreambuf_iterator<char>()));
  
  cl::Program::Sources sources(1, std::make_pair(src.c_str(), src.length() + 1));
  
  cl::Context context(myDevice);
  cl::Program program(context, sources);
  
  cl::CommandQueue queue(context, myDevice);  
  
  int szVec = 10;   
  
  float* A = new float[szVec];
  float* B = new float[szVec];
  
  randomInit(A,szVec);
  randomInit(B,szVec);
  
  float* C = new float[szVec];
  std::fill_n(C, szVec, 0);
  
  // create buffers on the device
  cl::Buffer buffer_A(context, CL_MEM_READ_WRITE, szVec * sizeof(float));
  cl::Buffer buffer_B(context, CL_MEM_READ_WRITE, szVec * sizeof(float));
  cl::Buffer buffer_C(context, CL_MEM_READ_WRITE, szVec * sizeof(float));
    
  // write arrays A and B to the device
  auto err = queue.enqueueWriteBuffer(buffer_A, CL_TRUE, 0, sizeof(float) * szVec, A);
  err = queue.enqueueWriteBuffer(buffer_B, CL_TRUE, 0,   sizeof(float) * szVec, B);
  
  if(err != CL_SUCCESS)
  {
    std::cout << "1Problem in writing data from Host to Device!, code: " << err << std::endl;
    exit(0);
  }
  
  err = program.build();
  
  if(err != CL_SUCCESS)
  {
    std::cout<<" Error building: "<<program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(myDevice)<< std::endl;
    exit(1);
  } else
  {
    std::cout<<" Successfully Build!" << std::endl;
  }

     
  // run the kernel
  cl::Kernel kernel(program,"vector_add", &err);
  if(err != CL_SUCCESS)
  {
    std::cout << "Problem in creating kernel!, code: " << err << std::endl;
    exit(0);
  }
 
  kernel.setArg(0, buffer_A);
  kernel.setArg(1, buffer_B);
  kernel.setArg(2, buffer_C);
  err = queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(szVec), cl::NullRange);

  if(err != CL_SUCCESS)
  {
    std::cout << "1Problem in enqueue!, code: " << err << std::endl;
    exit(0);
  }
 
  queue.finish();
  
  //read result C from the device to array C
  err = queue.enqueueReadBuffer(buffer_C, CL_TRUE, 0, sizeof(float) * szVec, C);
  if(err != CL_SUCCESS)
  {
    std::cout<<" Error in reading from Device, code:  " << err << std::endl;
    exit(1);
  } 

  std::cout << " result:" << std::endl;
 
  for(int i = 0; i < szVec; i++)
  {
    std::cout << A[i] << " + " << B[i] << " = " << C[i] << std::endl;
  }
  
  std::cout << std::endl;
 
  return 0;
} 
