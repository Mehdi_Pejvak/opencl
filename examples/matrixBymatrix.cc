
/*
 * @name   matrixBymatrix.cc
 * @info   The code developed to do matrix-matrix multipication
 *         on the GPU by using OpenCL library
 * @author Mehdi
*/


#define __CL_ENABLE_EXCEPTIONS
#define CL_PROFILING
#define CL_2D_WORKGROUP
//#define CL_LOCAL_MEM
#define CL_BLOCK

#define blkSize 8
#define WGS 16

#include <iostream>
#include <CL/cl.hpp>
#include <cassert>
#include <fstream>
#include <iostream>
#include <vector>
#include <string>

#ifndef CL_PROFILING
  #include <chrono>
#endif

#include "CL_ERROR.hpp"


int main()
{

  int i;   
  
  int mA;
  int nA;
  int mB;
  int nB;
  

  //get all platforms (drivers)
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);
    
  assert(platforms.size() > 0);
    
  std::cout << "_________________DEVICE INFO_________________" << std::endl;
  cl::Platform myPlatform = platforms[0];
  std::cout << "Using platform: " << myPlatform.getInfo<CL_PLATFORM_NAME>() << std::endl;
     
  // get default device of the default platform
  std::vector<cl::Device> devices;
  myPlatform.getDevices(CL_DEVICE_TYPE_GPU, &devices);
  
  assert(devices.size() > 0);
   
  cl::Device myDevice = devices[1];
  std::cout << "Using device: " << myDevice.getInfo<CL_DEVICE_NAME>() << std::endl;
  std::vector<::size_t> maxWorkItems;
  maxWorkItems = myDevice.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
  std::cout << "Max. Work Item Sizes: " << maxWorkItems[0] << "*" << maxWorkItems[1] << "*" << maxWorkItems[2] << std::endl;
  std::cout << "Max. Work Group Size: " << myDevice.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() << std::endl;
  std::cout << "Global Memory Size: " << myDevice.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>()
   / 1024/1024/1024 << " GB" << std::endl;
  std::cout << "Local Memory Size: " << myDevice.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>() 
    / 1024 << " KB" << std::endl;
  std::cout << "Constant Memory Size: " << myDevice.getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>() 
    / 1024 << " KB" << std::endl;
  std::cout << "Number of Compute Unit: " << myDevice.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << std::endl;

  std::cout << "_____________________________________________\n" << std::endl;


#ifdef CL_2D_WORKGROUP  
  std::ifstream matBymatFile("matrixBymatrix_2d_kernel.cl" );
#else
  std::ifstream matBymatFile("matrixBymatrix_kernel.cl" );
#endif

  std::string src(std::istreambuf_iterator<char>(matBymatFile), (std::istreambuf_iterator<char>()));
  
  cl::Program::Sources sources(1,std::make_pair(src.c_str(),src.length() + 1));
  
  cl::Context context(myDevice);
  cl::Program program(context, sources);

#ifdef CL_PROFILING
  // create queue to which we will push commands for the device.
  cl::CommandQueue queue(context, myDevice, CL_QUEUE_PROFILING_ENABLE);
#else
  cl::CommandQueue queue(context, myDevice);
#endif

    // A and B are the input matrices.
  // Two first rows of the file A.txt and B.txt are number of row and column
  // of matrices A and B. Matrices was written in lexogeraphcal order
 
  std::string Afname = "../data/A_l.txt";    
  std::ifstream AFile(Afname);

  std::string Bfname = "../data/B_l.txt";
  std::ifstream BFile(Bfname);
 
  if (!BFile.is_open() || !AFile.is_open())
  {
    std::cout << ">>> Problem in input file(s)" << std::endl;
    exit(1);
  }

  AFile >> mA;
  AFile >> nA;

  BFile >> mB;
  BFile >> nB;

  int mC = mA;
  int nC = nB;

  std::cout << "A(" << mA << "*" << nA << ")" << std::endl;
  std::cout << "B(" << mB << "*" << nB << ")" << std::endl;

  if ( nA != mB )
  {
    std::cout << ">>> Matrix and matrix are not compatible!" << std::endl;
    exit(1);
  }

  std::cout << "===> Creating Buffer"  << std::endl;
  // create buffers on the device
  cl::Buffer buffer_A(context, CL_MEM_READ_WRITE, mA * nA * sizeof(double));
  cl::Buffer buffer_B(context, CL_MEM_READ_WRITE, mB * nB * sizeof(double));
  cl::Buffer buffer_C(context, CL_MEM_READ_WRITE, mC * nC * sizeof(double));
  
  
  double* A = new double [mA * nA];
  double* B = new double [mB * nB];
  double* C = new double [mC * nC];  
  
  
  for (i = 0; i < mA * nA; ++i)
  {
    AFile >> *(A + i);
  }

  for (i = 0; i < mB * nB; ++i)
  {
    BFile >> *(B + i);
  }

  if (A == NULL || B == NULL)
  {
    std::cout << " Matrix A or B is NULL" << std::endl;
    exit(0);
  }


  // write arrays A and B to the device
  std::cout << "===> Write Data to Device"  << std::endl;
  try
  {
    queue.enqueueWriteBuffer(buffer_A, CL_TRUE, 0, mA * nA * sizeof(double), A);
    queue.enqueueWriteBuffer(buffer_B, CL_TRUE, 0, mB * nB * sizeof(double), B);
  } catch (cl::Error& error)
  {
    std::cout << ">>> Problem in writing data from Host to Device: " << getErrorString(error) << std::endl;
    exit(0);
  }

  std::string buildOptions;
  { // create preprocessor defines for the kernel
     char buf[256]; 
     sprintf(buf,"-D blkSize=%d", blkSize);
             buildOptions += std::string(buf);
  }

  // Build the program 
  std::cout << "===> Building the Program"  << std::endl;
  try
  {
    program.build(buildOptions.c_str());
    std::cout << "   ---> Build Successfully " << std::endl;
  } catch(cl::Error& error)
  {
    std::cout << ">>> Problem in building program: " << getErrorString(error) << std::endl;
    std::cout << "   --->" << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(myDevice) << std::endl;
    exit(0);
  }
 
  // run the kernel
  std::cout << "===> Creating Kernel"  << std::endl;
  cl::Kernel kernel;
  try
  {
#ifdef CL_LOCAL_MEM
     kernel = cl::Kernel(program, "mat_mul_local");
#elif def CL_BLOCK
     kernel = cl::Kernel(program, "mat_mul_Block");
#else
     kernel = cl::Kernel(program, "mat_mul");
#endif
  } catch(cl::Error& error)
  {
    std::cout << ">>> Problem in creating kernel: " << getErrorString(error) << std::endl;
    exit(0);
  }

  // Set the passing arguments of the kernel
  kernel.setArg(0, buffer_A);
  kernel.setArg(1, buffer_B);
  kernel.setArg(2, buffer_C);
  kernel.setArg(3, mA);
  kernel.setArg(4, nA);
  kernel.setArg(5, mB);
  kernel.setArg(6, nB);

#ifdef CL_LOCAL_MEM
  kernel.setArg(7, nA * sizeof(double), NULL);
 // kernel.setArg(8, nA * sizeof(double), NULL);
#endif

  // Specifying size of global and local work groups

  size_t localws[3] = {(size_t) WGS, (size_t) WGS, 1};
  size_t globalws[3] = {(size_t)nC, (size_t)mC, 1};
 
  
#ifndef CL_PROFILING 
  auto t1 = std::chrono::high_resolution_clock::now();
#endif
 
  std::cout << "===> Enqueue the Kernel"  << std::endl;
  cl::Event event;
  try
  {
#ifdef CL_2D_WORKGROUP
    queue.enqueueNDRangeKernel(kernel, cl::NullRange, 
        cl::NDRange(globalws[0], globalws[1],globalws[2]), 
        cl::NDRange(localws[0],localws[1], localws[2]), NULL, &event);
#else
    queue.enqueueNDRangeKernel(kernel, cl::NullRange, 
        cl::NDRange(1014), cl::NDRange(maxWorkUnit), NULL, &event);
#endif
  }catch (cl::Error& error)
  {
    std::cout << ">>> Problem in enqueue: " << getErrorString(error) << std::endl;
    exit(0);
  }

  try
  {
    queue.finish();
  }catch (cl::Error& error)
  {
    std::cout << ">>> Problem in finishing: " << getErrorString(error) << std::endl;
    exit(0);
  }

  std::cout << "===> Kernel Ran Successfully"  << std::endl;
#ifndef CL_PROFILING
  auto t2 = std::chrono::high_resolution_clock::now();
#endif

  // read result C from the device to array C
  std::cout << "===> Reading Data From Device"  << std::endl;
  try
  {
    queue.enqueueReadBuffer(buffer_C, CL_TRUE, 0, mC * nC * sizeof(double), C);
  } catch (cl::Error& error)
  {
    std::cout << ">>> Problem reading buffer in device: " << getErrorString(error) << std::endl;
    exit(0);
  }

  std::string Cfname = "../data/C.txt";
  std::ofstream CFile(Cfname);
 
  if (!CFile.is_open())
  {
    std::cout << "Problem in output file(s)" << std::endl;
    exit(1);
  }
  
  CFile << mC << std::endl;
  CFile << nC << std::endl;
  
  for(i = 0; i < mC; ++i)
  {
    for(int j = 0; j < nC; ++j)
    {
      CFile <<  *(C + i * nC + j) << std::endl;
    }
  }

  std::cout << "_____________________________________________" << std::endl;
  
  long double Seconds;
#ifdef CL_PROFILING
  cl_ulong time_start = event.getProfilingInfo<CL_PROFILING_COMMAND_START>();
  cl_ulong time_end =  event.getProfilingInfo<CL_PROFILING_COMMAND_END>();

  Seconds = (time_end-time_start) * 1.0e-9;
#else
  Seconds = std::chrono::duration_cast<std::chrono::milliseconds>(t2-t1).count() * 1e-3;

#endif

  std::cout << " OpenCl Execution time= " << Seconds << " seconds" << std::endl;
  std::cout << " Performance= " << 2.0 * mA * nA * nB / Seconds * 1.0e-9 << " GFlops" << std::endl;

  delete[] (A);
  delete[] (B);
  delete[] (C);
  AFile.close();
  BFile.close();
  CFile.close();
  
  return 0;
} 
