/*
 * @info  The function perfoms matrix-vector multipication Ax=y
*/
__kernel void matrixByvec(__global double* A, __global double* x, __global double* y, int mA, int nA)
{
  int my_id = get_global_id(0);
  
  for (int i = 0; i < nA; ++i)
  {
    *(y + my_id) += *(A + my_id * nA + i) * *(x + i);
  }
}


/*
 * @info  The function perfoms vector-vector multipication xy=z
*/
__kernel void vecByvec(__global double* x, __global double* y, __global double* z, int mx)
{
  

/*
 * @info  The function perfoms matrix-matrix multipication AB=C
*/
__kernel void matrixBymatrix(__global double *A, __global double* b, __global double* x, int mA, int nA , int mb) 
{
    // Get the index of the current element
    int myDim = get_global_id(0);
     
    int nC = nB;
    for(int i = 0; i < mA; ++i)
    {
      for (int j = 0; j < nA; ++j)
      {
        *(C + myDim * nC + i) += *(A + myDim * nA + j) * *(B + j * nB + i);
      }
    }
} 
