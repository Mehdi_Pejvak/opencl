 
#include "misc.hpp"
 
/*
 * the function used to roundup a given value to the closest multiplier of 
 * multiple value
 */
unsigned int roundUp (unsigned int value, unsigned int multiple)
{
  unsigned int remainder = value % multiple;

  if(remainder != 0)
  {
    value += (multiple - remainder);
  }

  return value;
}



/*
 * Source term on right hand side of the equation
 */
void set_domain_length(int init_height, int init_width, int& height, int& width, int step)
{
  height = floor(init_height / pow(2,step)) + 2;
  width  = floor(init_width  / pow(2,step)) + 2;
#ifdef DEBUG
   std::cout << "Step= " << step << ", Height= " << height << ", Width= " << width << std::endl;
#endif
}


/*
 * Source term on right hand side of the equation
 */
double f(const float& x, const float& y)
{
    return 2.0 * ((1 + x) * sin(x + y) - cos(x + y)) ;
}

// Diriclet boundary condition
double g(const float& x, const float& y)
{
    return (1.0 + x) * sin(x + y);
}


/*
 * initilize unknown matrix x
 */
void init_unknown(float* x, const int& height_x, const int& width_x, float value)
{
    for(int i = 0; i < height_x * width_x; ++i)
  { 
    x[i] = value;
  }
}


/*
 * Set the bounday condition
 */
void set_bndryCond(float* x, const int& width, const float& h)
{
  for(int i = 0; i < width; ++i)
  { 
    x[i] = 0;            // lower edge
    x[i * width] = 0;  // left edge
    x[i * width + width - 1] = g(h * width, i * h);          // right edge
    x[width * (width - 1) + i] = g( h * i,  width * h);      // upper edge
  }
}


/*
 * Set the right hand side
 */
void set_rightHandSide(float* b, const float& h, const int& width, const int& height)
{
    float x;
    float y;
    for (int i = 0; i < width * height; ++i)
    {
        x = (i%width) * h;
        y = floor(i/width) * h;
        b[i] = f(x,y);
    }
}


/*
 * The function loads the matrices (filters) and unknown matrix x 
 * by initila values and boundary condition in matrix x
 */
void create_data(float* x, float* A_matrix, float* A2h_matrix, float* F_matrix, float* F2h_matrix, float* prolong_matrix, float* restrict_matrix, const int& width_x, const int& height_x, const int& filterWidth, const float& h, const int& n, std::string& scheme)
{
  
  //set_rightHandSide(b,n,h,n+2,n+2);

  if (scheme =="jacobi")
  {
    // This two matrices are necessary for implementation of iterative schemes
    F_matrix[0] = 0.0;
    F_matrix[1] = 0.25;
    F_matrix[2] = 0.0;
    F_matrix[3] = 0.25;
    F_matrix[4] = 0.0;
    F_matrix[5] = 0.25;
    F_matrix[6] = 0.0;
    F_matrix[7] = 0.25;
    F_matrix[8] = 0.0;
  
    F2h_matrix[0] = 0.0;
    F2h_matrix[1] = 0.5; 
    F2h_matrix[2] = 0.0;
    F2h_matrix[3] = 0.5;
    F2h_matrix[4] = 0.0;
    F2h_matrix[5] = 0.5;
    F2h_matrix[6] = 0.0;
    F2h_matrix[7] = 0.5;
    F2h_matrix[8] = 0.0;
  }

  if (scheme =="sor")
  {
    double omega = 1.8;
    // This two matrices are necessary for implementation of iterative schemes
    F_matrix[0] = 0.0;
    F_matrix[1] = 0.25 * omega;
    F_matrix[2] = 0.0;
    F_matrix[3] = 0.25 * omega;
    F_matrix[4] = 0.0;
    F_matrix[5] = 0.25 * omega;
    F_matrix[6] = 0.0;
    F_matrix[7] = 0.25 * omega;
    F_matrix[8] = 0.0;
  
    F2h_matrix[0] = 0.0;
    F2h_matrix[1] = 0.5 * omega; 
    F2h_matrix[2] = 0.0;
    F2h_matrix[3] = 0.5 * omega;
    F2h_matrix[4] = 0.0;
    F2h_matrix[5] = 0.5 * omega;
    F2h_matrix[6] = 0.0;
    F2h_matrix[7] = 0.5 * omega;
    F2h_matrix[8] = 0.0;
  }

  // This two matrices (A and A2h) are necessary to caculate error
  A_matrix[0] = 0.0;
  A_matrix[1] = - 1.0;
  A_matrix[2] = 0.0;
  A_matrix[3] = - 1.0;
  A_matrix[4] = 4.0;
  A_matrix[5] = - 1.0;
  A_matrix[6] = 0.0;
  A_matrix[7] = - 1.0;
  A_matrix[8] = 0.0;
  
  A2h_matrix[0] = 0.0;
  A2h_matrix[1] = - 1.0; 
  A2h_matrix[2] = 0.0;
  A2h_matrix[3] = - 1.0;
  A2h_matrix[4] = - 2.0;
  A2h_matrix[5] = - 1.0;
  A2h_matrix[6] = 0.0;
  A2h_matrix[7] = - 1.0;
  A2h_matrix[8] = 0.0;
  
 
  // This two matrices are used to implement multigrid
  prolong_matrix[0] = 0.0;
  prolong_matrix[1] = 0.25;
  prolong_matrix[2] = 0.0;
  prolong_matrix[3] = 0.25;
  prolong_matrix[4] = 0.0;
  prolong_matrix[5] = 0.25;
  prolong_matrix[6] = 0.0;
  prolong_matrix[7] = 0.25;
  prolong_matrix[8] = 0.0;

  restrict_matrix[0] = 0.0625;
  restrict_matrix[1] = 0.125;
  restrict_matrix[2] = 0.0625;
  restrict_matrix[3] = 0.125;
  restrict_matrix[4] = 0.25;
  restrict_matrix[5] = 0.125;
  restrict_matrix[6] = 0.0625;
  restrict_matrix[7] = 0.125;
  restrict_matrix[8] = 0.0625;

}


/*
 * This function dispaly the info of the GPU device
 */
void cl_display_info(const cl::Platform& myPlatform, const cl::Device& myDevice)
{
  std::cout << "_______________DEVICE INFO_______________" << std::endl;  
  std::cout << "Using platform: " << myPlatform.getInfo<CL_PLATFORM_NAME>() << std::endl;
  std::cout << "Using device: " << myDevice.getInfo<CL_DEVICE_NAME>() << std::endl;
  std::vector<::size_t> maxWorkItems;
  maxWorkItems = myDevice.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
  std::cout << "Max. Work Item Sizes: " << maxWorkItems[0] << "*" << maxWorkItems[1] << "*" << maxWorkItems[2] << std::endl;
  std::cout << "Max. Work Group Size: " << myDevice.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() << std::endl;
  std::cout << "Global Memory Size: " << myDevice.getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>()
   / 1024/1024/1024 << " GB" << std::endl;
  std::cout << "Local Memory Size: " << myDevice.getInfo<CL_DEVICE_LOCAL_MEM_SIZE>() 
    / 1024 << " KB" << std::endl;
  std::cout << "Constant Memory Size: " << myDevice.getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>() 
    / 1024 << " KB" << std::endl;
  std::cout << "Number of Compute Unit: " << myDevice.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << std::endl;

  std::cout << "______________________________________\n" << std::endl;  
}


/*
 * The function print the given matrix
 */
void print_data(float* data, const int& height, const int& width)
{
   for (int i=0; i < height; ++i)
  {
    for (int j=0; j < width; ++j)
    {
      std::cout << std::setw(8) << *(data + i * width + j) << "\t";
    }
    std::cout << std::endl;
  }
}


/*
 * Write data into a file
 */
void write_file(std::string fname, float* data, int height, int width)
{
  std::ofstream file(fname);
 
  if (!file.is_open())
  {
    std::cout << "Problem in output file(s)" << std::endl;
    exit(1);
  }
  
  file << height << std::endl;
  file << width  << std::endl;
  
  for(int i = 0; i < height; ++i)
  {
    for(int j = 0; j < width; ++j)
    {
      file <<  *(data + i * width + j) << std::endl;
    }
  }
  file.close();
}


/*
 * Calculate norm of a vector
 */
void norm(const float* vec, const int& height, const int& width, double& vec_2)
{
  for (int i = 0; i < width * height; ++i)
  {
    vec_2 += pow(vec[i],2);
  }
  
  vec_2 = sqrt(vec_2);
}

/*
 * Calculate inf norm of a vector
 */
void norm_inf(const float* vec, const int& height, const int& width, double& vec_inf)
{
  for (int i = 0; i < width * height; ++i)
  {
    if (vec[i] > vec_inf) vec_inf = vec[i];
    
  }
}
