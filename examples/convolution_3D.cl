


__kernel void convolution_3D(__global float* x, __constant float* filter, 
                          __global float* y,
                          int rows, int cols, int depths, int filterWidth,
                          __local float* local_x, 
			  int localHeight, int localWidth, int localDepth)
{
   // padding for filter
   int filterRadius = (filterWidth / 2);
   int padding = filterRadius * 2;
  
   // size of work group in output
   int groupStartCol   = get_group_id(0) * get_local_size(0);
   int groupStartRow   = get_group_id(1) * get_local_size(1);
   int groupStartSlice = get_group_id(2) * get_local_size(2);

   // local ID for work-items
   int localCol   = get_local_id(0);
   int localRow   = get_local_id(1);
   int localSlice = get_local_id(2);

   // global ID of each work-item
   int globalCol   = groupStartCol   + localCol;
   int globalRow   = groupStartRow   + localRow;
   int globalSlice = groupStartSlice + localSlice;

   // Loop through every depth
   for (int k = localSlice; k < localDepth; k +=get_local_size(2))
   {
      int currSlice = groupStartSlice + k;
      // step down rows
      for (int i = localRow; i < localHeight; i += get_local_size(1))
      {
         int currRow = groupStartRow + i;

         // step across columns
         for (int j = localCol; j < localWidth; j += get_local_size(0))
         {
            int currCol = groupStartCol + j;

            // perform copy to local memory
            if (currRow < rows && currCol < cols && currSlice < depths)
            {
               *(local_x + k * (localWidth * localHeight) + i * localWidth + j) = 
                     *(x + currSlice * (rows * cols) + currRow * cols + currCol);
            }
         } 
      }
   }

   barrier(CLK_LOCAL_MEM_FENCE);

   // perform convolution
   if ((globalRow   < (rows   - padding)) && 
       (globalCol   < (cols   - padding)) && 
       (globalSlice < (depths - padding)))
   {     
     // Calculate right hand-side
     float sum = 0.0;

     int filterIdx = 0;
     int offset;
     
     for (int k = localSlice; k < localSlice + filterWidth; ++k)
     {
        for (int i = localRow; i < localRow + filterWidth; ++i)
        {
           offset = k * localWidth * localHeight + i * localWidth + localCol;
           sum += local_x[offset++] * filter[filterIdx++];
           sum += local_x[offset++] * filter[filterIdx++];
           sum += local_x[offset++] * filter[filterIdx++];
        }
     }
     *(y + (globalSlice + filterRadius) * cols * rows + 
           (globalRow   + filterRadius) * cols        + 
           (globalCol   + filterRadius)) = sum;
  }
}

