
#ifndef AUX_HPP
    #define __CL_ENABLE_EXCEPTIONS
    //#define CL_PROFILING
    //#define CL_READ4
    //#define CL_BUILD_OPTION

    #include <iostream>
    #include <CL/cl.hpp>
    #include <cassert>
    #include <fstream>
    #include <iostream>
    #include <vector>
    #include <string>
    #include <cstring>
    #include <cmath>
    #include <iomanip>

    #ifndef CL_PROFILING
    #include <chrono>
    #endif

    #include "CL_ERROR.hpp"


    unsigned int roundUp (unsigned int value, unsigned int multiple);
    void set_domain_length(int init_height, int init_width, int& height, int& width, int step);
    double f(const float& x, const float& y);
    double g(const float& x, const float& y);
    void init_unknown(float* x, const int& height_x, const int& width_x, float value);
    void set_bndryCond(float* x, const int& n, const float& h);
    void set_rightHandSide(float* b, const float& h, const int& width_b, const int& height_b);
    void create_data(float* x, float* A_matrix, float* A2h_matrix, float* F_matrix, float* F2h_matrix, float* prolong_matrix, float* restrict_matrix, const int& width_x, const int& height_x, const int& filterWidth, const float& h, const int& n, std::string& scheme);
    void cl_display_info(const cl::Platform& myPlatform, const cl::Device& myDevice);
    void print_data(float* data, const int& width, const int& height);
    void write_file(std::string fname, float* data, int height, int width);
    void norm(const float* vec, const int& height, const int& width, double& vec_2);
    void norm_inf(const float* vec, const int& height, const int& width, double& vec_inf);

#endif
