#ifndef ERROR_HPP
#define ERROR_HPP
  
#define __CL_ENABLE_EXCEPTIONS
#include <iostream>
#include <CL/cl.hpp>
#include <cassert>
#include <fstream>
#include <iostream>

	const char *getErrorString(cl::Error& error);
#endif
