__kernel void matrixByVec(__global double *A, __global double* x, __global double* b, int mA, int nA) 
{
    // Get the index of the current element
    int myDim = get_global_id(0);
    double tmp;

    for(int i = 0; i < nA; ++i)
    {
      tmp += *(A + myDim * nA + i) * *(x + i);
    }
    *(b + myDim) = tmp;
} 
