

/*
 * @name  convert_filter.cc
 * @info  The code developed to read filter data and convert it 
 *        to the corresponding iterative method
 *        (Jacobi, SOR) and write into output file
 *@author Mehdi
 */


#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>


using namespace std;

// Source term on right hand side of the equation
double f(const double& x, const double& y)
{
    return 2.0 * ((1 + x) * sin(x + y) - cos(x + y)) ;
}

// Diriclet boundary condition
double g(const double& x, const double& y)
{
    return (1.0 + x) * sin(x + y);
}

// Set the bounday condition
void set_bndryCond(double* x, const int& n, const double& h)
{
  for(int i = 0; i < n + 2; ++i)
  { 
    x[i] = 0;            // lower edge
    x[i * (n + 2)] = 0;  // left edge
    x[i * (n + 2) + n + 1] = g(h * (n + 2), i * h);  // right edge
    x[(n + 2) * (n + 1) + i] = g( h * i,  (n + 2) * h);      // upper edge
  }
}

void set_rightHandSide(double* b, const int& n, const double& h, const int& width_b, const int& height_b)
{
    double x;
    double y;
    for (int i = 0; i < width_b * height_b; ++i)
    {
        x = i * h;
        y = floor(i/width_b) * h;
        b[i] = f(x,y);
    }
}

int main(int argc, char* argv[])
{

  if(argc < 2)
  {
    cout << "Not enought input argument" << endl;
    exit(0);
  }

  double x_length = 1.0;
  double y_length = 1.0;
  int n = 100;

  double h = (x_length - 0.0) / (n + 2);

  int filterWidth = 3;

  double* x = new double[(n + 2) * (n + 2)];
  double* b = new double[(n + 2) * (n + 2)];
  double* prolong_matrix  = new double[filterWidth * filterWidth];
  double* restrict_matrix = new double[filterWidth * filterWidth];
  double* A_matrix        = new double[filterWidth * filterWidth];
  double* A2h_matrix      = new double[filterWidth * filterWidth];
  double* F_matrix        = new double[filterWidth * filterWidth];
  double* F2h_matrix      = new double[filterWidth * filterWidth];
  
  set_bndryCond(x,n,h);
  set_rightHandSide(b,n,h,n+2,n+2);

  if (strcmp(argv[1], "jacobi") == 0)
  {
    // This two matrices are necessary for implementation of iterative schemes
    F_matrix[0] = 0.0;
    F_matrix[1] = 0.25;
    F_matrix[2] = 0.0;
    F_matrix[3] = 0.25;
    F_matrix[4] = 0.0;
    F_matrix[5] = 0.25;
    F_matrix[6] = 0.0;
    F_matrix[7] = 0.25;
    F_matrix[8] = 0.0;
  
    F2h_matrix[0] = 0.0;
    F2h_matrix[1] = 0.5; 
    F2h_matrix[2] = 0.0;
    F2h_matrix[3] = 0.5;
    F2h_matrix[4] = 0.0;
    F2h_matrix[5] = 0.5;
    F2h_matrix[6] = 0.0;
    F2h_matrix[7] = 0.5;
    F2h_matrix[8] = 0.0;
  }

  if (strcmp(argv[1],"sor") == 0)
  {
    double omega = atof(argv[2]);
    // This two matrices are necessary for implementation of iterative schemes
    F_matrix[0] = 0.0;
    F_matrix[1] = 0.25 * omega;
    F_matrix[2] = 0.0;
    F_matrix[3] = 0.25 * omega;
    F_matrix[4] = 0.0;
    F_matrix[5] = 0.25 * omega;
    F_matrix[6] = 0.0;
    F_matrix[7] = 0.25 * omega;
    F_matrix[8] = 0.0;
  
    F2h_matrix[0] = 0.0;
    F2h_matrix[1] = 0.5 * omega; 
    F2h_matrix[2] = 0.0;
    F2h_matrix[3] = 0.5 * omega;
    F2h_matrix[4] = 0.0;
    F2h_matrix[5] = 0.5 * omega;
    F2h_matrix[6] = 0.0;
    F2h_matrix[7] = 0.5 * omega;
    F2h_matrix[8] = 0.0;
  }

    // This two matrices (A and A2h) are necessary to caculate error
  A_matrix[0] = 0.0;
  A_matrix[1] = - 1.0;
  A_matrix[2] = 0.0;
  A_matrix[3] = - 1.0;
  A_matrix[4] = 4.0;
  A_matrix[5] = - 1.0;
  A_matrix[6] = 0.0;
  A_matrix[7] = - 1.0;
  A_matrix[8] = 0.0;
  
  A2h_matrix[0] = 0.0;
  A2h_matrix[1] = - 0.25; 
  A2h_matrix[2] = 0.0;
  A2h_matrix[3] = - 0.25;
  A2h_matrix[4] = - 0.5;
  A2h_matrix[5] = - 0.25;
  A2h_matrix[6] = 0.0;
  A2h_matrix[7] = - 0.25;
  A2h_matrix[8] = 0.0;
  
 
  // This two matrices are used to implement multigrid
  prolong_matrix[0] = 0.0;
  prolong_matrix[1] = 0.25;
  prolong_matrix[2] = 0.0;
  prolong_matrix[3] = 0.25;
  prolong_matrix[4] = 0.0;
  prolong_matrix[5] = 0.25;
  prolong_matrix[6] = 0.0;
  prolong_matrix[7] = 0.25;
  prolong_matrix[8] = 0.0;

  restrict_matrix[0] = 0.0625;
  restrict_matrix[1] = 0.125;
  restrict_matrix[2] = 0.0625;
  restrict_matrix[3] = 0.125;
  restrict_matrix[4] = 0.25;
  restrict_matrix[5] = 0.125;
  restrict_matrix[6] = 0.0625;
  restrict_matrix[7] = 0.125;
  restrict_matrix[8] = 0.0625;


/* ___________Prolongation And Restriction Files___________*/
string xfname = "../data/x.txt";
  ofstream xFile(xfname);
  string bfname = "../data/b.txt";
  ofstream bFile(bfname);
  string prolong_filter = "../data/prolong_filter.txt";
  ofstream prolongFile(prolong_filter);
  string restrict_filter = "../data/restrict_filter.txt";
  ofstream restrictFile(restrict_filter);
  string F = "../data/F.txt";
  ofstream FFile(F);
  string F2h = "../data/F2h.txt";
  ofstream F2hFile(F2h);
  string A = "../data/A.txt";
  ofstream AFile(A);
  string A2h = "../data/A2h.txt";
  ofstream A2hFile(A2h);


  if (!xFile.is_open() || !bFile.is_open()
         || !prolongFile.is_open() || !restrictFile.is_open()
         || !A2hFile.is_open()  || !A2hFile.is_open() 
         || !FFile.is_open() || !F2hFile.is_open())
  { 
     cout << ">>> Problem in input file!" << endl;
     exit(1);
  }
 
  xFile << n+2 << endl;
  xFile << n+2  << endl;

  for (int i = 0; i < (n+2) * (n+2); ++i)
  {
    xFile << x[i] << endl;
  }
  
  bFile << n+2 << endl;
  bFile << n+2  << endl;

  for (int i = 0; i < (n+2) * (n+2); ++i)
  {
    bFile << b[i] << endl;
  }
  
  FFile << filterWidth << endl;
  FFile << filterWidth  << endl;

  for (int i = 0; i < filterWidth * filterWidth; ++i)
  {
    FFile << F_matrix[i] << endl;
  }

  F2hFile << filterWidth << endl;
  F2hFile << filterWidth  << endl;
 
  for (int i = 0; i < filterWidth * filterWidth; ++i)
  {
    F2hFile << F2h_matrix[i] << endl;
  }
  
  AFile << filterWidth << endl;
  AFile << filterWidth  << endl;

  for (int i = 0; i < filterWidth * filterWidth; ++i)
  {
    AFile << A_matrix[i] << endl;
  }

  A2hFile << filterWidth << endl;
  A2hFile << filterWidth  << endl;
 
  for (int i = 0; i < filterWidth * filterWidth; ++i)
  {
    A2hFile << A2h_matrix[i] << endl;
  }
  prolongFile << filterWidth << endl;
  prolongFile << filterWidth  << endl;

  for (int i = 0; i < filterWidth * filterWidth; ++i)
  {
    prolongFile << prolong_matrix[i] << endl;
  }

  restrictFile << filterWidth << endl;
  restrictFile << filterWidth  << endl;
 
  for (int i = 0; i < filterWidth * filterWidth; ++i)
  {
    restrictFile << restrict_matrix[i] << endl;
  }

  delete[] (x);
  delete[] (b);
  delete[] (F_matrix);
  delete[] (F2h_matrix);
  delete[] (A_matrix);
  delete[] (A2h_matrix);
  delete[] (prolong_matrix);
  delete[] (restrict_matrix);

  xFile.close();
  bFile.close();
  AFile.close();
  A2hFile.close();
  FFile.close();
  F2hFile.close();
  prolongFile.close();
  restrictFile.close();

  return 0;
}
