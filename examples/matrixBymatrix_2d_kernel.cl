


__kernel void mat_mul(__global double* A, __global double* B, __global double* C, int row_A, int col_A , int row_B, int col_B) 
{
    // Get the index of the current element
    int col = get_global_id(0);
    int row = get_global_id(1);
     

    double sum = 0.0f;
    
    for(int i = 0; i < col_A; ++i)
    {
        sum += *(A + row * col_A + i) * *(B + i * col_B + col);
    }
    *(C + row * col_B + col) = sum;
}



__kernel void mat_mul_local(__global double* A, __global double* B, __global double* C, int row_A, int col_A , int row_B, int col_B, __local double* local_A) 
{
    // Get the index of the current element
    int col = get_global_id(0);
    int row = get_global_id(1);

    for (int i =0; i < col_A; ++i)
    {
      local_A[i] = A[row * col_A + i];
//      local_B[i] = B[i * col_B + col];
    }
    
    //barrier(CLK_LOCAL_MEM_FENCE);         

    double sum = 0.0f;
    
    for(int i = 0; i < col_A; ++i)
    {
        sum += local_A[i] * *(B + i * col_B + col);
    }
    *(C + row * col_B + col) = sum;
}

__kernel void mat_mul_Block(__global double* A, __global double* B, __global double* C, int row_A, int col_A , int row_B, int col_B, __local double* local_A, __local double* local_B)
{
  // iterator
  int kloc;
  int Kblk;
  
  float tmp = 0.0f;
  
  // global location of C
  int global_i = get_global_id(0);
  int global_j = get_global_id(1);

  // group location
  int group_i = get_group_id(0);
  int group_j = get_group_id(1);

  // loxal location 
  int local_i = get_local_id(0);
  int local_j = get_local_id(1);

  // Number of blocks
  int numOfBlock = col_A / blkSize;

  // Starting point
  int A_base = group_i * col_A * blkSize;
  int B_base = group_j * blkSize;

  // increment step
  int A_inc = blkSize;
  int B_inc = blkSize * col_A;

  for (int Kblk = 0; Kblk < numOfBlock; ++Kblk)
  {
    local_A[local_i * blkSize * local_j] = A[A_base + local_i * col_A + local_j];
    local_B[local_i * blkSize * local_j] = B[B_base + local_i * col_A + local_j];

    barrier(CLK_LOCAL_MEM_FENCE);

    #pragma unroll
    for(kloc = 0; kloc < blkSize; ++kloc)
    {
      tmp += local_A[local_j * blkSize + kloc] * local_B[kloc * blkSize + local_i];
    }

    barrier(CLK_LOCAL_MEM_FENCE);
    A_base += A_inc;
    B_base += B_inc;
    
  }
    C[global_j * col_A + global_i] = tmp;
}
