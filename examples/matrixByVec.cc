
/*
 * @name   matrixByVec.cc
 * @info   The code developed to do matrix-vector multipication
 *         on the GPU by using OpenCL library
 * @author Mehdi
*/

#define __CL_ENABLE_EXCEPTIONS
#include <iostream>
#include <CL/cl.hpp>
#include <cassert>
#include <fstream>
#include <iostream>

int main()
{

  // get all platforms (drivers)
  std::vector<cl::Platform> platforms;
  cl::Platform::get(&platforms);
    
  assert(platforms.size() > 0);
    
  cl::Platform myPlatform = platforms[0];
  std::cout << "Using platform: " << myPlatform.getInfo<CL_PLATFORM_NAME>() << std::endl;
     
  // get default device of the default platform
  std::vector<cl::Device> devices;
  myPlatform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
  
  assert(devices.size() > 0);
  
  cl::Device myDevice = devices[0];
  std::cout << "Using device: " << myDevice.getInfo<CL_DEVICE_NAME>() << std::endl;
  std::vector<::size_t> maxWorkItems;
  maxWorkItems = myDevice.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
  std::cout << "Max Item: " << maxWorkItems[0] << "*" << maxWorkItems[1] << "*" << maxWorkItems[2] << std::endl;
  std::cout << "Max Compute Unit: " << myDevice.getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << std::endl;
  std::cout << "Max Work Group Size: " << myDevice.getInfo<CL_DEVICE_MAX_WORK_GROUP_SIZE>() << std::endl;
     
  std::ifstream matByVecFile("matrixByVec_kernel.cl" );
  std::string src(std::istreambuf_iterator<char>(matByVecFile), (std::istreambuf_iterator<char>()));
  
  cl::Program::Sources sources(1,std::make_pair(src.c_str(),src.length() + 1));
  
  cl::Context context(myDevice);
  cl::Program program(context, sources);
  
  // create queue to which we will push commands for the device.
  cl::CommandQueue queue(context,myDevice);

  int i;   
  int mA;
  int nA;
  int mx;
  
  // A is the input matrix and x is input vector.
  // Two first rows of the file A.txt are number of row and column
  // of matrix A and matrix A was written in lexogeraphcal order
  // In x.txt, first row is number of element in vector x.
  std::string Afname = "../data/A.txt";    
  std::ifstream AFile(Afname);

  std::string xfname = "../data/x.txt";
  std::ifstream xFile(xfname);
 

  if (!xFile.is_open() || !AFile.is_open())
  {
    std::cout << "Problem in input file(s)" << std::endl;
    exit(1);
  }

  AFile >> mA;
  AFile >> nA;

  xFile >> mx;
  
  std::cout << "A(" << mA << "*" << nA << ")" << std::endl;
  std::cout << "b(" << mx << ")" << std::endl;

  if ( mx != nA )
  {
    std::cout << "Matrix and vector are not compatible!" << std::endl;
    exit(1);
  }

  // create buffers on the device
  cl::Buffer buffer_A(context, CL_MEM_READ_WRITE, nA * mA * sizeof(double));
  cl::Buffer buffer_x(context, CL_MEM_READ_WRITE, mx *      sizeof(double));
  cl::Buffer buffer_b(context, CL_MEM_READ_WRITE, nA *      sizeof(double));
     
  double* A = new double [mA * nA];
  double* x = new double [mx];
  double* b = new double [nA];  
  
  //std::cout << "A" << std::endl;
  for (i = 0; i < mA * nA; ++i)
  {
    AFile >> *(A + i);
   // std::cout << *(A + i) << std::endl;
  }
  
  //std::cout << "x" << std::endl;
  for (i = 0; i < mx; ++i)
  {
    xFile >> *(x + i);
   // std::cout << *(x + i) << std::endl;
  }

  cl_int err;   
  // write arrays A and B to the device
  try
  {
    queue.enqueueWriteBuffer(buffer_A, CL_TRUE, 0, sizeof(double) * mA * nA, A);
    queue.enqueueWriteBuffer(buffer_x, CL_TRUE, 0,   sizeof(double) * mx, x);
   } catch (cl::Error& error)
  {
    std::cout << ">>> Problem in writing data from Host to Device: " << error.err() << std::endl;
    exit(0);
  }
  
  try
  {
    program.build();
  } catch (cl::Error& error)
  {
    std::cout << ">>> Problem in Building The Program: " << error.err() << std::endl;
    exit(0);
  }
 
  // run the kernel
  cl::Kernel kernel(program, "matrixByVec", &err);
  
  if (err != CL_SUCCESS)
  {
    std::cout << " Problem in kernel, code: " << err << std::endl;
    exit(0);
  }
  
  kernel.setArg(0, buffer_A);
  kernel.setArg(1, buffer_x);
  kernel.setArg(2, buffer_b);
  kernel.setArg(3, mA);
  kernel.setArg(4, nA);

  try
  {
    queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(mA), cl::NullRange);
   } catch (cl::Error& error)
  {
    std::cout << ">>> Problem in Enqueue The Kernel: " << error.err() << std::endl;
    exit(0);
  }

  queue.finish();
  
  // read result vector b from the device and store in array b
  try
  {
    queue.enqueueReadBuffer(buffer_b, CL_TRUE, 0, nA * sizeof(double), b);
  } catch (cl::Error& error)
  {
    std::cout << ">>> Problem in Reading Data From Device: " << error.err() << std::endl;
    exit(0);
  }
 
  std::string bfname = "../data/b.txt";
  std::ofstream bFile(bfname);
 

  if (!bFile.is_open())
  {
    std::cout << "Problem in output file(s)" << std::endl;
    exit(1);
  }

  int mb = mA;
  
  bFile << mb << std::endl;
 // std::cout <<"b=  " << std::endl ;
 
  for(i = 0; i < mb; ++i)
  {
//    std::cout << "|" <<  *(b + i) << "|" << std::endl;
    bFile << *(b + i) << std::endl;
  }
  
  delete[] (A);
  delete[] (x);
  delete[] (b);
  AFile.close();
  bFile.close();
  xFile.close();
  
     
  return 0;
} 

